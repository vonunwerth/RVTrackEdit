#ifndef _TEXT_H
#define _TEXT_H

#include "platform.h"
#include "TrackEditTypes.h"

typedef struct
{
	U8 Left;
	U8 Right;
}CHARMARGIN;

typedef struct
{
	U16					PixelWidth;
	U16					PixelHeight;
	U16					CharsPerRow;
	TPAGEID				Texture;
	CHARMARGIN*			Margins;
}FONTDESCRIPTION;

const U16 WHITE_FONT = 0;
const U16 RED_FONT = 128;

void DrawText(U16 x, U16 y, const char* text);
void DrawText(U16 x, U16 y, INDEX index);
void DrawMoreText(const char* text);
void DrawMoreText(INDEX index);
void SetCurrentFont(FONTDESCRIPTION* newfont);
void SetCharBase(U16 value);
U16	 FontWidth(void);
U16	 FontHeight(void);

#endif
