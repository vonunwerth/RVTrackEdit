#ifndef _PLATFORM_H
#define _PLATFORM_H

#ifdef _PC
#include "pcincl.h"
#endif

#include <bobtypes.h>
#include <typedefs.h>
#include "unitinfo.h"

//**********************************************
//
//**********************************************
//platform specific functions and data


//**********************************************
//
//**********************************************
enum TPAGEID
{
		TEX_UNITS_00,
		TEX_UNITS_01,
		TEX_UNITS_02,
		TEX_UNITS_03,
		TEX_UNITS_04,
		TEX_UNITS_05,
		TEX_UNITS_06,
		TEX_UNITS_07,
		TEX_UNITS_08,
		TEX_UNITS_09,
		TEX_UNITS_10,
		TEX_UNITS_11,
		TEX_UNITS_COUNT,
		TEX_SPRUE_BUTTONS = TEX_UNITS_COUNT,
		TEX_SPRUE_WIRE1,
		TEX_SPRUE_WIRE2,
		TEX_SPRUE_FONT,
		TEX_ALARM_CLOCK,
		TEX_ICONS,
		TEX_COUNT,					 //used for sizing the array of texture pointers
		TEX_NOT_ASSIGNED = TEX_COUNT //used to indicate that no texture is being used at present
};

void InitializeTextureCache(void);
void LoadBitmaps(void);
void SetCurrentTexture(TPAGEID newtexture);
void DrawSprite(REAL left, REAL top, REAL right, REAL bottom, REAL u0, REAL v0, REAL u1, REAL v1);
void SpriteClippingOff(void);
void SpriteClippingOn(void);
void SetSpriteColor(U32 color);
void InitializeSpritePoly(void);
void DrawSpinningModule(const TRACKTHEME* theme);
void DrawTrack(const TRACKTHEME* theme, const TRACKDESC* track);
void InitializeCursorPrimitive(void);
void DrawCursorBox(void);
void DrawUnitRoot(REAL h);
void InitializeUnitRootPrimitive(void);
void DrawPickupScreen(const TRACKTHEME* theme, const TRACKDESC* track);

//**********************************************
//
//**********************************************
enum SND_TYPE
{
	SND_QUERY,
	SND_MENU_OUT,
	SND_MENU_IN,
	SND_MENU_MOVE,
	SND_CONGRATS,
	SND_WARNING,
	SND_TICK_TOCK,
	SND_PLACE_MODULE,
	SND_DELETE,
	SND_ADJUST,
	SND_LOWER,
	SND_RAISE,
	SND_PLACE_PICKUP,
	SND_ROTATE,
	SND_TYPING,
	SND_COUNT	//not actually a sound but is used for looping through the list of sounds
};

void InitializeSoundSystem(void);
void StartSound(SND_TYPE sound);
void StartLoopingSound(SND_TYPE sound);
void StopSound(SND_TYPE sound);
void ShutdownSoundSystem(void);

//**********************************************
//
//**********************************************
enum
{
	BUTTON_UP = 0x00000001L,
	BUTTON_DOWN = 0x00000002L,
	BUTTON_LEFT = 0x00000004L,
	BUTTON_RIGHT = 0x00000008L,
	BUTTON_CONFIRM = 0x00000010L,
	BUTTON_CANCEL = 0x00000020L,
	BUTTON_PLACE_MODULE = 0x00000040L,
	BUTTON_PRIOR = 0x00000080L,
	BUTTON_NEXT = 0x00000100L,
	BUTTON_ROT_LEFT = 0x00000200L,
	BUTTON_ROT_RIGHT = 0x00000400L,
	BUTTON_DELETE = 0x00000800L,
	BUTTON_INSERT = 0x00001000L,
	BUTTON_SHIFT = 0x00002000L,
	BUTTON_PICKUP = 0x00004000L,
	BUTTON_NEXT_VARIANT = 0x00008000L,
	BUTTON_PREVIOUS_VARIANT = 0x00010000L,
	BUTTON_ROTATE_MODULE = 0x00020000L,
	BUTTON_ERASE = 0x00040000L,
	BUTTON_TEXTURE_TOGGLE = 0x0080000L,
};

typedef U32 BUTTON_STATES;

BOOL InitializeInputDevices(void);
void ShutdownInputDevices(void);
void AcquireInputStatus(void);
BOOL ButtonIsDown(BUTTON_STATES buttonmask);
BOOL ButtonJustWentDown(BUTTON_STATES buttonmask);

#endif
