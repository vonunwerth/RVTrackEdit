#include <assert.h>

#include "TrackEditTypes.h"	//NOTE - had to include this...
#include "states.h"			// to stop this from breaking the compile
#include "editorhelp.h"
#include "textstrings.h"

extern POPUP_MENU_STATES	PopupState;

static const int ACTION_OFFSET = 100;

static bool						HelpPresent = false;
static const HELP_DIALOG_INFO*	HelpInfo;

//**********************************************
//help for module selection mode
static const HELP_DIALOG_INFO  HelpDataChoosingGB = 
{
	TEXT_HELP_CHOOSING_KEY,
	TEXT_HELP_CHOOSING_ACTION,
	ACTION_OFFSET,
	160, 128,
	9, 5
};
static const HELP_DIALOG_INFO  HelpDataChoosingFR = 
{
	TEXT_HELP_CHOOSING_KEY,
	TEXT_HELP_CHOOSING_ACTION,
	ACTION_OFFSET,
	128, 176,
	11, 5
};
static const HELP_DIALOG_INFO  HelpDataChoosingDE = 
{
	TEXT_HELP_CHOOSING_KEY,
	TEXT_HELP_CHOOSING_ACTION,
	ACTION_OFFSET,
	160, 128,
	9, 5
};
static const HELP_DIALOG_INFO  HelpDataChoosingIT = 
{
	TEXT_HELP_CHOOSING_KEY,
	TEXT_HELP_CHOOSING_ACTION,
	ACTION_OFFSET,
	96, 128,
	11, 5
};
static const HELP_DIALOG_INFO  HelpDataChoosingES = 
{
	TEXT_HELP_CHOOSING_KEY,
	TEXT_HELP_CHOOSING_ACTION,
	ACTION_OFFSET,
	96, 128,
	11, 5
};

//**********************************************
//help for pickup placement mode
static const HELP_DIALOG_INFO  HelpDataPickupsGB = 
{
	TEXT_HELP_PICKUPS_KEY,
	TEXT_HELP_PICKUPS_ACTION,
	ACTION_OFFSET,
	320, 32,
	9, 4
};
static const HELP_DIALOG_INFO  HelpDataPickupsFR = 
{
	TEXT_HELP_PICKUPS_KEY,
	TEXT_HELP_PICKUPS_ACTION,
	ACTION_OFFSET + 64,
	256, 32,
	11, 4
};
static const HELP_DIALOG_INFO  HelpDataPickupsDE = 
{
	TEXT_HELP_PICKUPS_KEY,
	TEXT_HELP_PICKUPS_ACTION,
	ACTION_OFFSET + 32,
	256, 32,
	11, 4
};
static const HELP_DIALOG_INFO  HelpDataPickupsIT = 
{
	TEXT_HELP_PICKUPS_KEY,
	TEXT_HELP_PICKUPS_ACTION,
	ACTION_OFFSET,
	288, 32,
	10, 4
};
static const HELP_DIALOG_INFO  HelpDataPickupsES = 
{
	TEXT_HELP_PICKUPS_KEY,
	TEXT_HELP_PICKUPS_ACTION,
	ACTION_OFFSET,
	288, 32,
	10, 4
};

//**********************************************
//help for track adjustment mode
static const HELP_DIALOG_INFO  HelpDataAdjustingGB = 
{
	TEXT_HELP_ADJUSTING_KEY,
	TEXT_HELP_ADJUSTING_ACTION,
	ACTION_OFFSET,
	360, 24,
	8, 7
};
static const HELP_DIALOG_INFO  HelpDataAdjustingFR = 
{
	TEXT_HELP_ADJUSTING_KEY,
	TEXT_HELP_ADJUSTING_ACTION,
	ACTION_OFFSET + 32,
	296, 24,
	10, 7
};
static const HELP_DIALOG_INFO  HelpDataAdjustingDE = 
{
	TEXT_HELP_ADJUSTING_KEY,
	TEXT_HELP_ADJUSTING_ACTION,
	ACTION_OFFSET + 32,
	296, 24,
	10, 7
};
static const HELP_DIALOG_INFO  HelpDataAdjustingIT = 
{
	TEXT_HELP_ADJUSTING_KEY,
	TEXT_HELP_ADJUSTING_ACTION,
	ACTION_OFFSET + 32,
	296, 24,
	10, 7
};
static const HELP_DIALOG_INFO  HelpDataAdjustingES = 
{
	TEXT_HELP_ADJUSTING_KEY,
	TEXT_HELP_ADJUSTING_ACTION,
	ACTION_OFFSET + 32,
	328, 24,
	9, 7
};

//**********************************************
//help for save mode
static const HELP_DIALOG_INFO  HelpDataSavingGB = 
{
	TEXT_HELP_SAVING_KEY,
	TEXT_HELP_SAVING_ACTION,
	ACTION_OFFSET,
	160, 144,
	10, 5
};
static const HELP_DIALOG_INFO  HelpDataSavingFR = 
{
	TEXT_HELP_SAVING_KEY,
	TEXT_HELP_SAVING_ACTION,
	ACTION_OFFSET + 48,
	80, 144,
	15, 5
};
static const HELP_DIALOG_INFO  HelpDataSavingDE = 
{
	TEXT_HELP_SAVING_KEY,
	TEXT_HELP_SAVING_ACTION,
	ACTION_OFFSET + 32,
	112, 144,
	13, 5
};
static const HELP_DIALOG_INFO  HelpDataSavingIT = 
{
	TEXT_HELP_SAVING_KEY,
	TEXT_HELP_SAVING_ACTION,
	ACTION_OFFSET,
	96, 144,
	12, 5
};
static const HELP_DIALOG_INFO  HelpDataSavingES = 
{
	TEXT_HELP_SAVING_KEY,
	TEXT_HELP_SAVING_ACTION,
	ACTION_OFFSET,
	128, 144,
	12, 5
};

//**********************************************
//help for save mode
static const HELP_DIALOG_INFO  HelpDataLoadingGB = 
{
	TEXT_HELP_LOADING_KEY,
	TEXT_HELP_LOADING_ACTION,
	ACTION_OFFSET,
	192, 144,
	8, 5
};
static const HELP_DIALOG_INFO  HelpDataLoadingFR = 
{
	TEXT_HELP_LOADING_KEY,
	TEXT_HELP_LOADING_ACTION,
	ACTION_OFFSET,
	160, 144,
	10, 5
};
static const HELP_DIALOG_INFO  HelpDataLoadingDE = 
{
	TEXT_HELP_LOADING_KEY,
	TEXT_HELP_LOADING_ACTION,
	ACTION_OFFSET,
	160, 144,
	10, 5
};
static const HELP_DIALOG_INFO  HelpDataLoadingIT = 
{
	TEXT_HELP_LOADING_KEY,
	TEXT_HELP_LOADING_ACTION,
	ACTION_OFFSET,
	160, 144,
	10, 5
};
static const HELP_DIALOG_INFO  HelpDataLoadingES = 
{
	TEXT_HELP_LOADING_KEY,
	TEXT_HELP_LOADING_ACTION,
	ACTION_OFFSET,
	160, 144,
	10, 5
};

//**********************************************
//help for module placement mode
static const HELP_DIALOG_INFO  HelpDataPlacementGB = 
{
	TEXT_HELP_PLACING_KEY,
	TEXT_HELP_PLACING_ACTION,
	ACTION_OFFSET,
	320, 64,
	8, 8
};
static const HELP_DIALOG_INFO  HelpDataPlacementFR = 
{
	TEXT_HELP_PLACING_KEY,
	TEXT_HELP_PLACING_ACTION,
	ACTION_OFFSET + 64,
	128, 112,
	12, 8
};
static const HELP_DIALOG_INFO  HelpDataPlacementDE = 
{
	TEXT_HELP_PLACING_KEY,
	TEXT_HELP_PLACING_ACTION,
	ACTION_OFFSET + 32,
	192, 112,
	12, 8
};
static const HELP_DIALOG_INFO  HelpDataPlacementIT = 
{
	TEXT_HELP_PLACING_KEY,
	TEXT_HELP_PLACING_ACTION,
	ACTION_OFFSET,
	288, 64,
	9, 8
};
static const HELP_DIALOG_INFO  HelpDataPlacementES = 
{
	TEXT_HELP_PLACING_KEY,
	TEXT_HELP_PLACING_ACTION,
	ACTION_OFFSET,
	320, 64,
	8, 8
};

//**********************************************
//help for popout menu
static const HELP_DIALOG_INFO  HelpDataMenuGB = 
{
	TEXT_HELP_MENU_KEY,
	TEXT_HELP_MENU_ACTION,
	ACTION_OFFSET,
	160, 128,
	9, 4
};
static const HELP_DIALOG_INFO  HelpDataMenuFR = 
{
	TEXT_HELP_MENU_KEY,
	TEXT_HELP_MENU_ACTION,
	ACTION_OFFSET,
	96, 128,
	11, 4
};
static const HELP_DIALOG_INFO  HelpDataMenuDE = 
{
	TEXT_HELP_MENU_KEY,
	TEXT_HELP_MENU_ACTION,
	ACTION_OFFSET,
	160, 128,
	9, 4
};
static const HELP_DIALOG_INFO  HelpDataMenuIT = 
{
	TEXT_HELP_MENU_KEY,
	TEXT_HELP_MENU_ACTION,
	ACTION_OFFSET,
	96, 128,
	11, 4
};
static const HELP_DIALOG_INFO  HelpDataMenuES = 
{
	TEXT_HELP_MENU_KEY,
	TEXT_HELP_MENU_ACTION,
	ACTION_OFFSET,
	96, 128,
	11, 4
};

//**********************************************
//help for popout menu
static const HELP_DIALOG_INFO  HelpDataQueryGB = 
{
	TEXT_HELP_QUERY_KEY,
	TEXT_HELP_QUERY_ACTION,
	ACTION_OFFSET,
	160, 128,
	9, 4
};
static const HELP_DIALOG_INFO  HelpDataQueryFR = 
{
	TEXT_HELP_QUERY_KEY,
	TEXT_HELP_QUERY_ACTION,
	ACTION_OFFSET + 48,
	128, 128,
	11, 4
};
static const HELP_DIALOG_INFO  HelpDataQueryDE = 
{
	TEXT_HELP_QUERY_KEY,
	TEXT_HELP_QUERY_ACTION,
	ACTION_OFFSET,
	160, 128,
	9, 4
};
static const HELP_DIALOG_INFO  HelpDataQueryIT = 
{
	TEXT_HELP_QUERY_KEY,
	TEXT_HELP_QUERY_ACTION,
	ACTION_OFFSET,
	160, 128,
	9, 4
};
static const HELP_DIALOG_INFO  HelpDataQueryES = 
{
	TEXT_HELP_QUERY_KEY,
	TEXT_HELP_QUERY_ACTION,
	ACTION_OFFSET,
	160, 128,
	9, 4
};

static const HELP_DIALOG_INFO*  HelpID[HELP_ID_MAX_INDEX][LANGUAGE_COUNT] =
{
	{&HelpDataChoosingGB, &HelpDataChoosingFR, &HelpDataChoosingDE, &HelpDataChoosingIT, &HelpDataChoosingES},
	{&HelpDataPickupsGB, &HelpDataPickupsFR, &HelpDataPickupsDE, &HelpDataPickupsIT, &HelpDataPickupsES},
	{&HelpDataAdjustingGB, &HelpDataAdjustingFR, &HelpDataAdjustingDE, &HelpDataAdjustingIT, &HelpDataAdjustingES},
	{&HelpDataSavingGB, &HelpDataSavingFR, &HelpDataSavingDE, &HelpDataSavingIT, &HelpDataSavingES},
	{&HelpDataLoadingGB, &HelpDataLoadingFR, &HelpDataLoadingDE, &HelpDataLoadingIT, &HelpDataLoadingES},
	{&HelpDataPlacementGB, &HelpDataPlacementFR, &HelpDataPlacementDE, &HelpDataPlacementIT, &HelpDataPlacementES},
	{&HelpDataMenuGB, &HelpDataMenuFR, &HelpDataMenuDE, &HelpDataMenuIT, &HelpDataMenuES},
	{&HelpDataQueryGB, &HelpDataQueryFR, &HelpDataQueryDE, &HelpDataQueryIT, &HelpDataQueryES},
};

INDEX	ScreenStateHelp[] =
{
	HELP_ID_CHOOSING_MODULE,
	HELP_ID_PLACING_PICKUPS,
	HELP_ID_ADJUSTING_TRACK,
	HELP_ID_SAVING_TRACK,
	MAX_INDEX,		//No help reqd for Creating a track
	HELP_ID_LOADING_TRACK,
	MAX_INDEX,		//No help reqd for Exporting a track
	MAX_INDEX,		//No help reqd for Quitting a track
	HELP_ID_PLACING_MODULE,
	MAX_INDEX,		//No help reqd for Deleting a track
};

void ShowHelp(void)
{
	if(!ErrorExists())
	{
		if(!NotificationExists())
		{
			if(QueryExists())
			{
				HelpPresent = true;
				HelpInfo = HelpID[HELP_ID_QUERY][GetLanguage()];
			}
			else
			{
				if(PopupState == PMS_INACTIVE)
				{
					INDEX helpindex = ScreenStateHelp[GetScreenState()];
					if(helpindex != MAX_INDEX)
					{
						HelpPresent = true;
						HelpInfo = HelpID[helpindex][GetLanguage()];
					}
				}
				else
				{
					HelpPresent = true;
					HelpInfo = HelpID[HELP_ID_MENU][GetLanguage()];
				}
			}
		}
	}
}

void HideHelp(void)
{
	HelpPresent = false;
}

bool HelpExists(void)
{
	return HelpPresent;
}

const HELP_DIALOG_INFO* CurrentHelp(void)
{
	return HelpInfo;
}
