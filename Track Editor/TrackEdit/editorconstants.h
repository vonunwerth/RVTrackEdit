#ifndef _EDITORCONSTANTS_H
#define _EDITORCONSTANTS_H

#include <constants.h>

const REAL SMALL_CUBE_SIZE = GameScale * Real(4.0f);
const REAL SMALL_CUBE_HALF = SMALL_CUBE_SIZE / Real(2.0f);
const REAL SMALL_CUBE_SPACING = SMALL_CUBE_SIZE * Real(1.25f);
const REAL SMALL_CUBE_RADIUS = ((REAL)sqrt((SMALL_CUBE_SIZE * SMALL_CUBE_SIZE) * Real(3.0f))) / Real(2.0f);
const REAL BIG_CUBE_SIZE = SMALL_CUBE_SIZE * Real(4.0f);
const REAL BIG_CUBE_RADIUS = ((REAL)sqrt((BIG_CUBE_SIZE * BIG_CUBE_SIZE) * Real(3.0f))) / Real(2.0f);
const REAL COLLISION_BLEED = GameScale * Real(0.40f);

const REAL CameraHeight = SMALL_CUBE_SIZE * Real(7.5f);
const REAL CameraDistance = ((REAL)sqrt((CameraHeight * CameraHeight) * Real(2.0f)));
const REAL ElevationStep = GameScale / 2;
const U16  PopupSlideDistance	= 185;
const REAL PopupMenuDuration = 0.25f;
const REAL ArrowFlashTime = 0.2f;

#endif