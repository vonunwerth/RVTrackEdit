#ifndef _RENDER_H
#define _RENDER_H

#include "UnitInfo.h"

#include "text.h"

const U32 PURE_RED = 0xFF0000L;
const U32 PURE_GREEN = 0x00FF00L;
const U32 PURE_BLUE = 0x0000FFL;
const U32 PURE_WHITE = 0xFFFFFFL;

void	SetSpriteColor(U32 color);
void	DrawPlanViewOfTrack(const TRACKTHEME* theme);
void	DrawHelpInfo(void);
void	DrawErrorMessage(void);
void	DrawQueryMessage(void);
void	DrawNotificationMessage(void);
void	DrawExportingScreen(void);
void	DrawMainSprue(void);
void	DrawLoadMenu(void);
void	DrawPopupMenu(void);
void	DrawSaveMenu(void);
void	DrawSprite(REAL left, REAL top, REAL right, REAL bottom, REAL u0, REAL v0, REAL u1, REAL v1);
void	DrawPlacingScreen(const TRACKTHEME* theme);
void	DrawChoosingScreen(const TRACKTHEME* theme);
void	DrawAdjustmentScreen(const TRACKTHEME* theme);
void	DrawFrame(REAL xpix, REAL ypix, U16 cellwidth, U16 cellheight);
void	DrawFilledFrame(REAL xpix, REAL ypix, U16 cellwidth, U16 cellheight);
void	InitializeCursorPrimitive(void);
void	InitializeUnitRootPrimitive(void);
void	CalculateWallMatrices(const TRACKDESC* track);

#endif //_RENDER_H