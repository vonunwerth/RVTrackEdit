#ifndef _EDITOR_H
#define _EDITOR_H

#include <bobtypes.h>
#include "TrackEditTypes.h"

bool	DecreaseElevation(void);
bool	IncreaseElevation(void);
void	RotateModule(void);
void	PlaceModuleNumber(TRACKDESC* track, INDEX moduleid, U16 xpos, U16 ypos);
void	PlaceModuleNumber(TRACKDESC* track, INDEX moduleid);
void	PlaceModule(TRACKDESC* track);
void	EraseModulesUnitsFromTrack(TRACKDESC* track, U16 xpos, U16 ypos);
void	DeleteModule(TRACKDESC* track);
void	SetCurrentModule(INDEX moduleid);
void	SetTrackCursorSize(void);
void	MakeTrackFromModules(TRACKDESC* track);
void	UpdateTrackAndCursor(TRACKDESC* track);
void	CorrectCursor(CURSORDESC* cursor);
bool	CreateTrack(TRACKDESC* track, U32 width, U32 height, const char* name);
void	DestroyTrack(TRACKDESC* track);
bool	CreateTrackAndCursor(TRACKDESC* track, U32 width, U32 height, const char* name);
bool	CloneTrack(TRACKDESC* desttrack, const TRACKDESC* srctrack);
void	ClonePickups(TRACKDESC* desttrack, const TRACKDESC* srctrack);
void	CopyTrackRegion(TRACKDESC* desttrack, const TRACKDESC* srctrack, U16 destx, U16 desty, U16 srcx, U16 srcy, U16 srcwidth, U16 srcheight);
void	NewTrack(TRACKDESC* track);
bool	SaveTrack(TRACKDESC* track);
bool	SelectModuleUnderCursor(TRACKDESC* track);
void	DecreaseTrackWidth(TRACKDESC* track);
void	IncreaseTrackWidth(TRACKDESC* track);
void	DecreaseTrackHeight(TRACKDESC* track);
void	IncreaseTrackHeight(TRACKDESC* track);
void	SlideTrackRight(TRACKDESC* track);
void	SlideTrackLeft(TRACKDESC* track);
void	SlideTrackUp(TRACKDESC* track);
void	SlideTrackDown(TRACKDESC* track);
bool	NextVariant(void);
bool	PreviousVariant(void);
bool	ToggleSurface(void);
void	CopyCursorInfo(CURSORDESC* dest, const CURSORDESC* src);
bool	TogglePickup(TRACKDESC* track, U16 xpos, U16 ypos);
void	ShiftPickups(TRACKDESC* track, S16 xoffset, S16 yoffset);
void	ErasePickup(TRACKDESC* track, U16 xpos, U16 ypos);
U16		ModuleWidth(TRACKMODULE* module);
U16		ModuleHeight(TRACKMODULE* module);

const	U16 DEFAULT_TRACK_WIDTH  = 16;
const	U16 DEFAULT_TRACK_HEIGHT = 16;

const	U16 MAX_TRACK_WIDTH  = 20;
const	U16 MAX_TRACK_HEIGHT = 20;

const	U16 MIN_TRACK_WIDTH  = 10;
const	U16 MIN_TRACK_HEIGHT = 10;

#endif
