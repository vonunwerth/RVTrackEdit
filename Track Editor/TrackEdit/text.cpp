#include <assert.h>

#include "Render.h"
#include "platform.h"
#include "textstrings.h"

CHARMARGIN	SprueFontMargins[] = {
								{7,6}, {6,6}, {5,4}, {5,5}, {4,4}, {5,4}, {7,6}, {7,6},
								{7,6}, {6,6}, {5,4}, {7,6}, {6,6}, {7,6}, {5,5}, {5,4},

								{6,6}, {5,4}, {5,4}, {5,4}, {5,4}, {5,4}, {5,5}, {5,4},
								{5,4}, {7,6}, {7,6}, {5,4}, {6,5}, {5,4}, {6,4}, {3,2},

								{5,4}, {5,4}, {5,4}, {5,5}, {6,5}, {6,5}, {5,4}, {5,5},
								{7,6}, {5,3}, {5,4}, {6,5}, {3,3}, {5,5}, {5,4}, {5,4},

								{5,4}, {5,4}, {5,4}, {5,5}, {5,4}, {5,4}, {3,3}, {5,4},
								{5,4}, {5,5}, {6,5}, {5,5}, {6,5}, {5,5}, {5,4}, {7,7},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {6,6}, {5,5}, {5,5},
								{7,6}, {7,6}, {5,5}, {7,6}, {4,3}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {6,6}, {5,5}, {5,5}, {4,4}, {5,5},
								{5,5}, {5,5}, {6,6}, {7,7}, {6,6}, {6,6}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{2,2}, {3,4}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},

								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {5,5},
								};

FONTDESCRIPTION SprueWire1Font = {32, 32, 8, TEX_SPRUE_WIRE1, NULL};
FONTDESCRIPTION SprueWire2Font = {32, 32, 8, TEX_SPRUE_WIRE2, NULL};
FONTDESCRIPTION SprueTextFont = {16, 16, 16, TEX_SPRUE_FONT, SprueFontMargins};

static FONTDESCRIPTION* CurrentFont = NULL;

//storage for the last printed at position - so that printing can be continued from last cursor position
static U16				TextX = 0;
static U16				TextY = 0;
static U16				TextMargin = 0;
static U16				CharBase = 0;
static unsigned char	SpecialChars[] = "���������������������񺿡ߌ�";
static const unsigned char FIRST_SPECIAL_CHAR = '\x80';

unsigned char ConvertSpecialChar(unsigned char special)
{
	unsigned char n = 0;
	while(SpecialChars[n] != '\0')
	{
		if(SpecialChars[n] == special)
		{
			return n + FIRST_SPECIAL_CHAR;
		}
		n++;
	}
	return '?';	//return standard value for chars which aren't in the table
}

void SetCurrentFont(FONTDESCRIPTION* newfont)
{
	assert(newfont != NULL);
	
	SetCurrentTexture(newfont->Texture);
	CurrentFont = newfont;
}

U16 FontWidth(void)
{
	assert(CurrentFont != NULL);
	
	return CurrentFont->PixelWidth;
}

U16 FontHeight(void)
{
	assert(CurrentFont != NULL);
	
	return CurrentFont->PixelHeight;
}

void SetCharBase(U16 value)
{
	CharBase = value;
}

void RenderText(U16 x, U16 y, const char* text)
{
	assert(CurrentFont != NULL);
	
	SetCurrentTexture(CurrentFont->Texture);
	REAL destleft = x;
	REAL desttop = y;
	REAL destright = destleft + CurrentFont->PixelWidth;
	REAL destbottom = desttop + CurrentFont->PixelHeight;
	U16 n = 0;
	U16 leftmargin = 0;
	U16 rightmargin = 1;

	while(text[n] != '\0')
	{
		unsigned char c = (unsigned char)text[n];
		
		if(c == '\n')
		{
			desttop += CurrentFont->PixelHeight;
			destbottom += CurrentFont->PixelHeight;
		}
		else
		{
			if(c == '\r')
			{
				destleft = TextMargin;
				destright = destleft + CurrentFont->PixelWidth;
				rightmargin = 1;
			}
			else
			{
				if(c == ' ')	//character was a space
				{
					rightmargin = CurrentFont->PixelWidth;	//so give the previous character a bigger margin
				}
				else
				{
					if(c >= FIRST_SPECIAL_CHAR)
					{
						c = ConvertSpecialChar(c);
					}
					c -= '!';
					if( c >= '\0') 
					{
						U16 adjustedchar = c + CharBase;
						REAL srcleft = (adjustedchar % CurrentFont->CharsPerRow) * CurrentFont->PixelWidth;
						REAL srctop = (adjustedchar / CurrentFont->CharsPerRow) * CurrentFont->PixelHeight;
						REAL srcright = srcleft + CurrentFont->PixelWidth;
						REAL srcbottom = srctop + CurrentFont->PixelHeight;
						if(CurrentFont->Margins != NULL)
						{
							S16 shiftfactor = rightmargin + CurrentFont->Margins[c].Left;
							shiftfactor -= 1;	//leave a gap between letters
							destleft -= shiftfactor;
							destright -= shiftfactor;
							rightmargin = CurrentFont->Margins[c].Right;
						}
						srcleft /= 256.0f;
						srctop /= 256.0f;
						srcright /= 256.0f;
						srcbottom /= 256.0f;
						DrawSprite(destleft, desttop, destright, destbottom, srcleft, srctop, srcright, srcbottom);
					}
				}
				destleft += CurrentFont->PixelWidth;
				destright += CurrentFont->PixelWidth;
			}
		}
		n++;
	}
	TextX = destleft;
	TextY = desttop;
}

void DrawText(U16 x, U16 y, const char* text)
{
	assert(CurrentFont != NULL);
	assert(text != NULL);
	
	TextMargin = x;
	RenderText(x, y, text);
}

void DrawText(U16 x, U16 y, INDEX index)
{
	DrawText(x, y, GetTextString(index));
}

void DrawMoreText(const char* text)
{
	assert(CurrentFont != NULL);

	RenderText(TextX, TextY, text);	
}

void DrawMoreText(INDEX index)
{
	DrawMoreText(GetTextString(index));
}
