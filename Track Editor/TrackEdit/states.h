#ifndef _STATES_H
#define _STATES_H

#include <typedefs.h>
#include "TrackEditTypes.h"

const U16	MAX_ERROR_LENGTH = 2048;
const U16	MAX_NOTIFICATION_LENGTH = 2048;

bool ErrorExists(void);
void FlagError(const char* message);
void FlagError(INDEX textindex);
void CancelError(void);
const char* GetErrorText(void);

bool NotificationExists(void);
void FlagNotification(const char* message);
void FlagNotification(INDEX textindex);
void CancelNotification(void);
const char* GetNotificationText(void);

bool QueryExists(void);
void FlagQuery(const char* message, bool initialstate);
void FlagQuery(INDEX textindex, bool initialstate);
void CancelQuery(void);
const char* GetQueryText(void);
void PreviousQueryState(void);
void NextQueryState(void);
void SetQueryToYes(void);
void SetQueryToNo(void);
bool QuerySetToYes(void);

void SetScreenState(EDIT_SCREEN_STATES newstate);
void PopScreenState(void);
EDIT_SCREEN_STATES GetScreenState(void);

void HeartBeat(REAL elapsedtime);

#endif
