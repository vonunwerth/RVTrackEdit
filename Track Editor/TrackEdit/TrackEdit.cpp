//-----------------------------------------------------------------------------
// File: TrackEdit.cpp
//
// Desc: Main application logic for the Re-Volt Track Editor
//
//       Note: This code uses the D3D Framework helper library.
//
//
// Copyright (c) 1998 Probe Entertainment. All rights reserved.
//-----------------------------------------------------------------------------

#include <assert.h>
#include <math.h>
#include <string>

#include "platform.h"
#include "fileio.h"
#include "editor.h"
#include "Render.h"
#include "inputs.h"
#include "states.h"
#include "modules.h"
#include "textstrings.h"
#include "editorhelp.h"

//-----------------------------------------------------------------------------
// Declare the application globals for use by WinMain.cpp
//-----------------------------------------------------------------------------
TCHAR*				g_strAppTitle       = TEXT( "Re-Volt Track Editor" );
BOOL				g_bAppUseZBuffer    = TRUE;    // Create/use z-buffering
BOOL				g_bAppUseBackBuffer = TRUE;    // Create/use a back buffer
BOOL				g_bAppUseRefRast    = FALSE;   // Enumerate the reference rasterizer
BOOL				g_bWindowed			= FALSE;

//-----------------------------------------------------------------------------
// Declare the other application globals
//-----------------------------------------------------------------------------
LPDIRECT3DDEVICE3		D3DDevice;
TRACKTHEME				Theme;
TRACKDESC				TrackData;
TRACKDESC				UndoTrack;

CURSORDESC				PickupCursor;
CURSORDESC				ModuleCursor;
INDEX					CurrentModule;
CURSORDESC				TrackCursor;
DIRECTION				ModuleDirection;
S16						ModuleElevation;
string					RTUPathName;
DXUNIT**				DXUnits				= NULL;
char**					FileList			= NULL;
U16						FileCount;
U16						FileWindowStart		= 0;
U16						FileWindowOffset	= 0;

PLACEMENT_CAMERA_DIR	PlacementViewpoint	= PCD_NORTH;
REVOLTVECTOR			Viewpoints[4] = {
										{ZERO, -CameraHeight, -CameraHeight},
										{CameraHeight, -CameraHeight, ZERO},
										{ZERO, -CameraHeight, CameraHeight},
										{-CameraHeight, -CameraHeight, ZERO},
										};
REVOLTVECTOR			CameraPos;
REAL					CameraVelocity;
REAL					CameraDeflection;

//-----------------------------------------------------------------------------
// Defines, constants, and global variables
//-----------------------------------------------------------------------------
extern MODULE_LOOKUP_INFO ToyLookup;
extern HINSTANCE	g_hInst;
extern HWND			g_hWnd;

//-----------------------------------------------------------------------------
// Function prototypes and global (or static) variables
//-----------------------------------------------------------------------------
VOID    AppPause( BOOL );
HRESULT App_InitDeviceObjects( LPDIRECT3DDEVICE3, LPDIRECT3DVIEWPORT3 );
VOID    App_DeleteDeviceObjects( LPDIRECT3DDEVICE3, LPDIRECT3DVIEWPORT3 );
void	App_SetResolution(DWORD width, DWORD height, DWORD depth);
void	CreatePrimitives(void);
void	InitializeTheme(void);
void	DestroyTheme(void);
void	DestroyPrimitives(void);
void	AcquireInputStatus(void);
void	Handle_Save_Name_Typing(HWND hWnd, int vKeyCode);	//PC only 
void	Handle_Query_Typing(HWND hWnd, int vKeyCode);	//PC only 

//-----------------------------------------------------------------------------
// File scope variables
//-----------------------------------------------------------------------------

static D3DMATRIX	proj;

static Light		*lpLight1,
					*lpLight2;

static Material		*lpBackgroundMat,
					*lpGridMat,
					*lpUnitMat;

//-----------------------------------------------------------------------------
// Name: App_InitOptions()
// Desc: Called during initial app startup, this function should setup global
//       variables to indicate the options desired by the app.
//       i.e. g_bWindowed = FALSE etc
//-----------------------------------------------------------------------------
VOID App_InitOptions( )
{
	g_bWindowed = FALSE;
	int i = __argc - 1;
	while(i--)
	{
		if(stricmp(__argv[i+1], "-window") == 0)
		{
			g_bWindowed = TRUE;
		}
		if(strnicmp(__argv[i+1], "-L", 2) == 0)
		{
			SetLanguage((INDEX)atoi(&__argv[i+1][2]));
		}
	}
	App_SetResolution(640, 480, 16);
}

//-----------------------------------------------------------------------------
// Name: App_OneTimeSceneInit()
// Desc: Called during initial app startup, this function performs all the
//       permanent initialization.
//-----------------------------------------------------------------------------
HRESULT App_OneTimeSceneInit( HWND hWnd, LPDIRECT3DDEVICE3 pd3dDevice,
                              LPDIRECT3DVIEWPORT3 pvViewport )
{

#ifdef _DEBUG
	RTUPathName = ".";
#else
	char application_name[_MAX_PATH];
	GetModuleFileName(g_hInst, application_name, sizeof(application_name));
	RTUPathName = application_name;
	RTUPathName = RTUPathName.substr(0, RTUPathName.rfind('\\'));

#endif

	InitializeTheme();
	
	D3DDevice = pd3dDevice;	//set up the (one and only) global D3D device
	
	if(ReadRTUFile(RTUPathName, &Theme) != false)
	{
		Theme.Lookup = &ToyLookup;
		TrackData.Units = NULL;
		TrackData.Modules = NULL;

		InitializeCursorPrimitive();
		InitializeUnitRootPrimitive();
		InitializeSpritePoly();
		CreatePrimitives();
		LoadBitmaps();
		PlacementViewpoint	= PCD_NORTH;
		CameraPos = Viewpoints[PlacementViewpoint];
		CameraDeflection = CameraVelocity = ZERO;

		ModuleCursor.X = 0;
		ModuleCursor.Y = 0;
		ModuleCursor.XSize = 1;
		ModuleCursor.YSize = 1;

		ModuleCursor.XMax = 2;
		ModuleCursor.YMax = Theme.Lookup->GroupCount;
		ModuleCursor.AbsMax = ModuleCursor.YMax;

		ModuleDirection = NORTH;
		ModuleElevation = 0;
		
		//create a default track
		NewTrack(&TrackData);

		// Initialize the device-dependant objects, such as materials and lights.
		// Note: this is in a separate function so that device changes can be
		// handled easily.
		return App_InitDeviceObjects( pd3dDevice, pvViewport );
	}
	else
	{
		MessageBox( hWnd, TEXT("Initialization failed\n\nThis app will now exit."),
					RTUPathName.c_str(), MB_ICONERROR|MB_OK );
		return E_FAIL;
	}
}

//-----------------------------------------------------------------------------
// Name: App_FrameMove()
// Desc: Called once per frame, the call is the entry point for animating
//       the scene.
//-----------------------------------------------------------------------------
HRESULT App_FrameMove( LPDIRECT3DDEVICE3 pd3dDevice, FLOAT fTimeKey )
{
	static bool firstpass = true;
	static FLOAT lasttime;
	FLOAT elapsedtime = 0;
	if(firstpass == false)
	{
		elapsedtime = fTimeKey - lasttime;
	}
	lasttime = fTimeKey;
	firstpass = false;

	HeartBeat(elapsedtime);

    return DD_OK;
}

//-----------------------------------------------------------------------------
// Name: App_Render()
// Desc: Called once per frame, the call is the entry point for 3d
//       rendering. This function sets up render states, clears the
//       viewport, and renders the scene.
//-----------------------------------------------------------------------------
HRESULT App_Render( LPDIRECT3DDEVICE3 pd3dDevice, 
                    LPDIRECT3DVIEWPORT3 pvViewport,
                    D3DRECT* prcViewportRect )
{
    //Clear the viewport
    pvViewport->Clear( 1UL, prcViewportRect, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER );

    AcquireInputStatus();
	
	// Begin the scene 
    if( SUCCEEDED( pd3dDevice->BeginScene() ) )
    {

		DWORD			dwSurfW, dwSurfH;

		// Get Surface Width and Height
		dwSurfW = abs (prcViewportRect->x2 - prcViewportRect->x1);
		dwSurfH = abs (prcViewportRect->y2 - prcViewportRect->y1);

		REAL inv_aspect = ONE;
		
		if (dwSurfW)
		{
			inv_aspect = (REAL)dwSurfH/(REAL)dwSurfW;
		}

		D3DUtil_SetProjectionMatrix(proj, pi / Real(4.0f), inv_aspect, Real(1000.0f), Real(50000.0f));
		proj._22 = -proj._22;	//adjust projection matrix so that our right handed data looks correct

		pd3dDevice->SetTransform(D3DTRANSFORMSTATE_PROJECTION, &proj);

		DrawMainSprue();
        if(ErrorExists())
		{
			DrawErrorMessage();
		}
		else
		{
			if(QueryExists())
			{
				DrawQueryMessage();
			}
			else
			{
				if(NotificationExists())
				{
					DrawNotificationMessage();
				}
				else
				{
					switch(GetScreenState())
					{
						case ESS_PLACING_MODULE:
							DrawPlacingScreen(&Theme);
							DrawPopupMenu();
						break;
						
						case ESS_CHOOSING_MODULE:
							DrawChoosingScreen(&Theme);
						break;

						case ESS_PLACING_PICKUPS:
							DrawPickupScreen(&Theme, &TrackData);
						break;

						case ESS_LOADING_TRACK:
							DrawLoadMenu();
						break;

						case ESS_SAVING_TRACK:
							DrawSaveMenu();
						break;

						case ESS_EXPORTING_TRACK:
							DrawExportingScreen();
						break;			
						
						case ESS_ADJUSTING_TRACK:
							DrawAdjustmentScreen(&Theme);
						break;
					}
				}
			}
		}
		if(HelpExists())
		{
			DrawHelpInfo();
		}
        // End the scene.
        pd3dDevice->EndScene();
    }

    return DD_OK;
}




//-----------------------------------------------------------------------------
// Name: App_InitDeviceObjects()
// Desc: Initialize scene objects.
//-----------------------------------------------------------------------------
HRESULT App_InitDeviceObjects( LPDIRECT3DDEVICE3 pd3dDevice,
                               LPDIRECT3DVIEWPORT3 pvViewport )
{
    // Check parameters
    if( NULL==pd3dDevice || NULL==pvViewport )
        return DDERR_INVALIDPARAMS;

    LPDIRECT3D3 pD3D;
    if( FAILED( pd3dDevice->GetDirect3D( &pD3D ) ) )
        return E_FAIL;

    pD3D->Release();

	// Create and set up the background material
	lpBackgroundMat = new Material(pD3D, pd3dDevice);
	lpBackgroundMat->SetDiffuse(D3DVECTOR(Real(0.097f), Real(0.195f), Real(0.39f)));
	lpBackgroundMat->SetAsBackground(pvViewport);

	lpUnitMat = new Material(pD3D, pd3dDevice);
	lpUnitMat->SetDiffuse(D3DVECTOR(ONE, ONE, ONE));
	lpUnitMat->SetRampSize(2);

	// Create the unit textures and attach them to the materials
	D3DTextr_RestoreAllTextures(pd3dDevice);
    pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
    pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
    pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );

	InitializeTextureCache();
	InitializeSoundSystem();
		
	// create 2 lights
	D3DVECTOR	color(ONE, ONE, ONE);
	D3DVECTOR	direction(Real(-0.5f), -ONE, Real(-0.3f));

	lpLight1 = new DirectionalLight(pD3D, color, Normalize(direction));
	if (lpLight1) {
		lpLight1->AddToViewport(pvViewport);
	}

	lpLight2 = new DirectionalLight(pD3D, color / Real(2.0f), -Normalize(direction));
	if (lpLight2) {
		lpLight2->AddToViewport(pvViewport);
	}

	lpUnitMat->SetAsCurrent(pd3dDevice);
	
	// set renderstates back to default
    if (pd3dDevice->SetRenderState(D3DRENDERSTATE_BLENDENABLE, FALSE) != D3D_OK)
        return FALSE;
    if (pd3dDevice->SetRenderState(D3DRENDERSTATE_SRCBLEND, D3DBLEND_ONE) != D3D_OK)
        return FALSE;
    if (pd3dDevice->SetRenderState(D3DRENDERSTATE_DESTBLEND, D3DBLEND_ZERO) != D3D_OK)
		return FALSE;
    if (pd3dDevice->SetRenderState(D3DRENDERSTATE_ZENABLE, TRUE) != D3D_OK)
		return FALSE;
    if (pd3dDevice->SetRenderState(D3DRENDERSTATE_ZWRITEENABLE, TRUE) != D3D_OK)
		return FALSE;
    if (pd3dDevice->SetRenderState(D3DRENDERSTATE_CULLMODE,D3DCULL_CW) != D3D_OK)
		return FALSE;
    if (pd3dDevice->SetRenderState(D3DRENDERSTATE_COLORKEYENABLE, TRUE) != D3D_OK)
		return FALSE;

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: App_FinalCleanup()
// Desc: Called before the app exits, this function gives the app the chance
//       to cleanup after itself.
//-----------------------------------------------------------------------------
HRESULT App_FinalCleanup( LPDIRECT3DDEVICE3 pd3dDevice, 
                          LPDIRECT3DVIEWPORT3 pvViewport)
{
    ShutdownInputDevices();
	ShutdownSoundSystem();
	DestroyPrimitives();
	DestroyTheme();
	DestroyTrack(&TrackData);
	App_DeleteDeviceObjects( pd3dDevice, pvViewport );
    
    return DD_OK;
}




//-----------------------------------------------------------------------------
// Name: App_DeleteDeviceObjects()
// Desc: Called when the app is exitting, or the device is being changed,
//       this function deletes any device dependant objects.
//-----------------------------------------------------------------------------
VOID App_DeleteDeviceObjects( LPDIRECT3DDEVICE3 pd3dDevice, 
                              LPDIRECT3DVIEWPORT3 pvViewport)
{
	D3DTextr_InvalidateAllTextures();

	// Cleanup lights
	if (lpLight1) {
		if (pvViewport) {
			lpLight1->RemoveFromViewport(pvViewport);
		}

	    delete lpLight1;
		lpLight1 = NULL;
	}

#if 1
	if (lpLight2) {
		if (pvViewport) {
			lpLight2->RemoveFromViewport(pvViewport);
		}

		delete lpLight2;
		lpLight2 = NULL;
	}
#endif

	// Cleanup Materials
	delete lpBackgroundMat;
	delete lpUnitMat;

	lpBackgroundMat = NULL;
	lpUnitMat = NULL;


    D3DTextr_InvalidateAllTextures();
}




//----------------------------------------------------------------------------
// Name: App_RestoreSurfaces
// Desc: Restores any previously lost surfaces. Must do this for all surfaces
//       (including textures) that the app created.
//----------------------------------------------------------------------------
HRESULT App_RestoreSurfaces( )
{
    D3DTextr_RestoreAllTextureSurfaces();
    return D3D_OK;
}




//-----------------------------------------------------------------------------
// Name: App_ConfirmDevice()
// Desc: Called during device intialization, this code checks the device
//       for some minimum set of capabilities
//-----------------------------------------------------------------------------
HRESULT App_ConfirmDevice( DDCAPS* pddDriverCaps,
						   D3DDEVICEDESC* pd3dDeviceDesc )
{
    return S_OK;
}

/*
**-----------------------------------------------------------------------------
**  Name:       CreatePrimitives
**  Purpose:	Contructs D3D version of the data which has been loaded into
				memory from an RTU (Revolt Track Unit) file
**-----------------------------------------------------------------------------
*/
#ifdef _DEBUG
	static U32 memused = 0;
	#define COUNT_ALLOC(n) (memused += (n))
	#define RESET_ALLOC_COUNT (memused = 0)
#else
	#define COUNT_ALLOC(n)
	#define RESET_ALLOC_COUNT
#endif

void CreatePrimitives(void)
{
	RESET_ALLOC_COUNT;
	DXUnits = new DXUNIT*[Theme.UnitCount];
	COUNT_ALLOC(sizeof(DXUNIT*) * Theme.UnitCount);
	static REAL ucoords[] = {Real(0.0), Real(1.0), Real(1.0), Real(0.0)};
	static REAL vcoords[] = {Real(0.0), Real(0.0), Real(1.0), Real(1.0)};
	static BASICPOLY blankuvpoly = {{0, 0, 0, 0},FALSE};
	for(U32 n = 0; n < Theme.UnitCount; n++)
	{
		DXUNIT* dxunit = new DXUNIT;
		COUNT_ALLOC(sizeof(DXUNIT));
		DXUnits[n] = dxunit;
		TRACKUNIT* unit = Theme.Units[n];
		MESH* mesh = &Theme.Meshes[unit->MeshID];
		dxunit->ComponentCount = Theme.Meshes[n].PolySetCount;
		dxunit->Components = new COMPONENT*[dxunit->ComponentCount];
		COUNT_ALLOC(sizeof(COMPONENT*) * dxunit->ComponentCount);
		for(U16 c = 0; c < dxunit->ComponentCount; c++)
		{
			COMPONENT* component = new COMPONENT;
			COUNT_ALLOC(sizeof(COMPONENT));
			dxunit->Components[c] = component;
			component->PrimitiveCount = Theme.TPageCount;
			component->Primitives = new PRIMITIVE*[component->PrimitiveCount];
			COUNT_ALLOC(sizeof(PRIMITIVE*) * component->PrimitiveCount);

			for(U16 ps = 0; ps < component->PrimitiveCount; ps++)
			{
				PRIMITIVE* primitive = new PRIMITIVE;
				COUNT_ALLOC(sizeof(PRIMITIVE));
				component->Primitives[ps] = primitive;
				BASICPOLY* uvpoly = &blankuvpoly;
				U32 vc = 0;
				if(c == PAN_INDEX)
				{
					for(U16 p = 0; p < Theme.PolySets[mesh->PolySetIndices[c]].PolygonCount; p++)
					{
						BASICPOLY* poly = &Theme.Polys[Theme.PolySets[mesh->PolySetIndices[c]].Indices[p] & ~GOURAUD_SHIFTED];
						if((unit->UVPolys[p].TPageID & ~UV_REVERSED) == ps)
						{
							vc += 3;
							if(poly->IsTriangle == false)
							{
								vc += 3;
							}
						}
					}
				}
				else
				{
					if(ps == 0)
					{
						vc = Theme.PolySets[mesh->PolySetIndices[c]].VertexCount;
					}
				}
				primitive->VertexCount = vc;
				if(vc == 0)
				{
					primitive->Vertices = NULL;
				}
				else
				{
					primitive->Vertices = new D3DVERTEX[primitive->VertexCount];
					COUNT_ALLOC(sizeof(D3DVERTEX) * primitive->VertexCount);
					U16 pv = 0;
					for(U16 p = 0; p < Theme.PolySets[mesh->PolySetIndices[c]].PolygonCount; p++)
					{
						if(((c == PAN_INDEX) && ((unit->UVPolys[p].TPageID & ~UV_REVERSED) == ps)) || ((c != PAN_INDEX) && (ps == 0)) )
						{
#ifdef _ORIGINAL_SOURCE
							BASICPOLY* poly = &Theme.Polys[Theme.PolySets[mesh->PolySetIndices[cEM.Indices[p] & ~GOURAUD_SHIFTED];
#else
							BASICPOLY* poly = &Theme.Polys[Theme.PolySets[mesh->PolySetIndices[c]].Indices[p] & ~GOURAUD_SHIFTED];
#endif
							uvpoly = &blankuvpoly;
							U32 uvmodvalue = 4;
							U32 uvindex = 0;
							U32 uvstep = 1;
							if(c == PAN_INDEX)
							{
								uvpoly = &Theme.UVPolys[unit->UVPolys[p].PolyID];
								uvmodvalue = uvpoly->IsTriangle ? 3 : 4;
								uvindex = unit->UVPolys[p].Rotation;
								uvstep = (unit->UVPolys[p].TPageID & UV_REVERSED) ? (uvmodvalue - 1) : 1;
							}
							D3DVECTOR v1;
							D3DVECTOR v2;
							D3DVECTOR normal;
							VECTOR_SUBTRACT(&v1, &Theme.Verts[poly->Vertices[0]], &Theme.Verts[poly->Vertices[1]]);
							VECTOR_SUBTRACT(&v2, &Theme.Verts[poly->Vertices[2]], &Theme.Verts[poly->Vertices[1]]);
							CROSS_PRODUCT(&normal, &v1, &v2);
							NORMALIZE_VECTOR(&normal);

							for(U16 v = 0; v < 3; v++)
							{
								primitive->Vertices[pv].x = Theme.Verts[poly->Vertices[v]].X;
								primitive->Vertices[pv].y = Theme.Verts[poly->Vertices[v]].Y;
								primitive->Vertices[pv].z = Theme.Verts[poly->Vertices[v]].Z;
								primitive->Vertices[pv].nx = normal.x;
								primitive->Vertices[pv].ny = normal.y;
								primitive->Vertices[pv].nz = normal.z;
								primitive->Vertices[pv].tu = Theme.UVCoords[uvpoly->Vertices[uvindex]].U;
								primitive->Vertices[pv].tv = Theme.UVCoords[uvpoly->Vertices[uvindex]].V;
								pv++;
								uvindex += uvstep;
								uvindex %= uvmodvalue;
							}
							if(!poly->IsTriangle)
							{
								primitive->Vertices[pv].x = primitive->Vertices[pv-3].x;
								primitive->Vertices[pv].y = primitive->Vertices[pv-3].y;
								primitive->Vertices[pv].z = primitive->Vertices[pv-3].z;
								primitive->Vertices[pv].nx = normal.x;
								primitive->Vertices[pv].ny = normal.y;
								primitive->Vertices[pv].nz = normal.z;
								primitive->Vertices[pv].tu = primitive->Vertices[pv-3].tu;
								primitive->Vertices[pv].tv = primitive->Vertices[pv-3].tv;
								pv++;
								primitive->Vertices[pv].x = primitive->Vertices[pv-2].x;
								primitive->Vertices[pv].y = primitive->Vertices[pv-2].y;
								primitive->Vertices[pv].z = primitive->Vertices[pv-2].z;
								primitive->Vertices[pv].nx = normal.x;
								primitive->Vertices[pv].ny = normal.y;
								primitive->Vertices[pv].nz = normal.z;
								primitive->Vertices[pv].tu = primitive->Vertices[pv-2].tu;
								primitive->Vertices[pv].tv = primitive->Vertices[pv-2].tv;
								pv++;
								primitive->Vertices[pv].x = Theme.Verts[poly->Vertices[3]].X;
								primitive->Vertices[pv].y = Theme.Verts[poly->Vertices[3]].Y;
								primitive->Vertices[pv].z = Theme.Verts[poly->Vertices[3]].Z;
								primitive->Vertices[pv].nx = normal.x;
								primitive->Vertices[pv].ny = normal.y;
								primitive->Vertices[pv].nz = normal.z;
								primitive->Vertices[pv].tu = Theme.UVCoords[uvpoly->Vertices[uvindex]].U;
								primitive->Vertices[pv].tv = Theme.UVCoords[uvpoly->Vertices[uvindex]].V;
								pv++;
							}
						}
					}
				}
			}
		}
	}
}
  
void DestroyPrimitives(void)
{
	if(DXUnits != NULL)
	{
		for(U32 n = 0; n < Theme.UnitCount; n++)
		{
			DXUNIT* dxunit = DXUnits[n];
			for(U16 c = 0; c < dxunit->ComponentCount; c++)
			{
				COMPONENT* component = dxunit->Components[c];
				component->PrimitiveCount = Theme.TPageCount;

				for(U16 ps = 0; ps < component->PrimitiveCount; ps++)
				{
					delete[] component->Primitives[ps]->Vertices;
					delete component->Primitives[ps];
				}
				delete[] component->Primitives;
				delete dxunit->Components[c];
			}
			delete[] dxunit->Components;
			delete DXUnits[n];
		}
		delete[] DXUnits;
	}
}
  
void InitializeTheme(void)
{
	memset(&Theme, 0, sizeof(TRACKTHEME));
}

void DestroyTheme(void)
{
	if(Theme.Modules != NULL)
	{
		TRACKMODULE* module;
		while(Theme.ModuleCount--)
		{
			module = Theme.Modules[Theme.ModuleCount];
			if(module != NULL)
			{
				RevoltTrackUnitInstance* instance;
				while(module->InstanceCount--)
				{
					instance = module->Instances[module->InstanceCount];
					delete instance;
				}
				delete[] module->Instances;
				TRACKZONE* zone;
				while(module->ZoneCount--)
				{
					zone = module->Zones[module->ZoneCount];
					delete zone;
				}
				delete[] module->Zones;
				U16 g = MAX_MODULE_ROUTES;
				while(g--)
				{
					AINODEINFO* node;
					while(module->NodeCount[g]--)
					{
						node = module->Nodes[g][module->NodeCount[g]];
						delete node;
					}
					delete []module->Nodes[g];
				}
				LIGHTINFO* light;
				while(module->LightCount--)
				{
					light = module->Lights[module->LightCount];
					delete light;
				}
				delete[] module->Lights;
			}
			delete Theme.Modules[Theme.ModuleCount];
		}
		delete[] Theme.Modules;
		Theme.Modules = 0;
	}

	if(Theme.Units != NULL)
	{
		TRACKUNIT* unit;
		while(Theme.UnitCount--)
		{
			unit = Theme.Units[Theme.UnitCount];
			if(unit != NULL)
			{
				delete[] unit->UVPolys;
				delete[] unit->Surfaces;
				delete[] unit->RGBs;
				delete[] unit;
			}
		}
		delete[] Theme.Units;
		Theme.Units = 0;
	}

	if(Theme.Meshes != NULL)
	{
		MESH* mesh;
		while(Theme.MeshCount--)
		{
			mesh = &Theme.Meshes[Theme.MeshCount];
			delete[] mesh->PolySetIndices;
		}
		delete[] Theme.Meshes;
		Theme.Modules = 0;
	}

	POLYSET* polyset;
	while(Theme.PolySetCount--)
	{
		polyset = &Theme.PolySets[Theme.PolySetCount];
		delete[] polyset->Indices;
	}
	delete[] Theme.PolySets;
	delete[] Theme.Polys;
	delete[] Theme.UVPolys;
	delete[] Theme.Verts;
	delete[] Theme.UVCoords;
	delete[] Theme.RGBPolys;
	
	Theme.UnitCount = 0;
	Theme.UVCoordCount = 0;
	Theme.UVPolyCount = 0;
	Theme.VertCount = 0;
	Theme.PolyCount = 0;
	Theme.MeshCount = 0;
	Theme.ModuleCount = 0;
	Theme.TPageCount = 0;
}
 
void QuitApp(void)
{
	assert(g_hWnd != NULL);

	PostMessage(g_hWnd, WM_CLOSE, 0, 0);
}

LRESULT App_HandleKeystroke(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	int vKeyCode = (int)wParam;
	switch(msg)
	{
		case WM_KEYDOWN:
			if(QueryExists())
			{
				Handle_Query_Typing(hWnd, vKeyCode);
			}
			else
			{
				switch(GetScreenState())
				{
					case ESS_SAVING_TRACK:
						Handle_Save_Name_Typing(hWnd, vKeyCode);
					break;
				}
			}
		break;
	}
	return 0;
}

