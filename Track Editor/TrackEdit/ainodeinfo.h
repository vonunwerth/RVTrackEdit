//
// Typedefs and structures
//

typedef struct {
	long Speed;
	VEC Pos;
} ONE_AINODE;


typedef struct _AINODE {
	char Priority, StartNode, pad[2];
	REAL RacingLine, FinishDist;
	long RacingLineSpeed, CentreSpeed;
	struct _AINODE *Prev[MAX_AINODE_LINKS];
	struct _AINODE *Next[MAX_AINODE_LINKS];
	ONE_AINODE Node[2];

	long ZoneID;
	VEC	Centre, RVec;

	struct _AINODE *ZonePrev;
	struct _AINODE *ZoneNext;
} AINODE;

typedef struct {
	long Count;
	AINODE *FirstNode;
} AINODE_ZONE;

typedef struct {
	char Priority, StartNode, pad[2];
	float RacingLine, FinishDist, fpad[2];
	long RacingLineSpeed, CentreSpeed;
	long Prev[MAX_AINODE_LINKS];
	long Next[MAX_AINODE_LINKS];
	ONE_AINODE Node[2];
} FILE_AINODE;
