#ifndef _OUTPUTUNITFILE_H
#define _OUTPUTUNITFILE_H

#include "MappedVector.h"
#include "Primitives.h"
#include <Fname.h>
#include <endstrm.h>

//const RTU_WRITE_VERSION = 1;		//version 1 - now includes UV information
//const RTU_WRITE_VERSION = 2;		//version 2 - now includes surface material information
//const RTU_WRITE_VERSION = 3;		//version 3 - now includes track zone information
//const RTU_WRITE_VERSION = 4;		//version 4 - no longer contains VALIDEXIT fields
//const RTU_WRITE_VERSION = 5;		//version 5 - now includes a pickup position for each unit
//const U16 RTU_WRITE_VERSION = 6;	//version 6 - now includes a AI Node info for each module
//const U16 RTU_WRITE_VERSION = 7;	//version 7 - now includes wall geometry
//const U16 RTU_WRITE_VERSION = 8;	//version 8 - now includes gouraud information
//const U16 RTU_WRITE_VERSION = 9;	//version 9 - now includes root edge requirement data for each unit
//const U16 RTU_WRITE_VERSION = 10;	//version 10 - now optimized for size (using U16 instead of U32, etc)
//const U16 RTU_WRITE_VERSION = 11;	//version 11 - now pools polysets instead of storing them directly in meshes
//const U16 RTU_WRITE_VERSION = 12;	//version 12 - now includes light information for each module
const U16 RTU_WRITE_VERSION = 13;	//version 13 - Fixed alignment problems

class UnitFileOutput
{
	protected:
		RevoltTheme* Theme;
		EndianOutputStream* Output;
	public:
		UnitFileOutput();
		virtual void OutputVertices(void);
		virtual void OutputPolygons(void);
		virtual void OutputRGBPolys(void);
		virtual void OutputMeshes(void);
		virtual void OutputUVCoords(void);
		virtual void OutputUVPolys(void);
		virtual void OutputUnits(void);
		virtual void OutputModules(void);
		virtual void OutputTPages(void);
		virtual void OutputPolySet(const RevoltPolySet* polyset);
		virtual void OutputPolySets(void);
		virtual void OutputMesh(const RevoltMesh* mesh);
		virtual void OutputModule(const RevoltModule* module);
		virtual void OutputUnitFile(const string& filename, RevoltTheme* theme, EndianStream::Endian whichend);
};

class CN64TPage;
class UnitFileOutputN64 : public UnitFileOutput {
public:

	void OutputUnitFile(const string& filename, RevoltTheme* theme, EndianStream::Endian whichend);
	void OutputUnits(void);
	void OutputVertices(void);
	void OutputUVCoords(void);
	void OutputTPages(void);

	bool BreakTPages(void);

	int			numTPages;
	CN64TPage	*n64TPages;

};








#endif
