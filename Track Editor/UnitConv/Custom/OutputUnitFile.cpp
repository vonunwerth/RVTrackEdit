//-------------------------------------------------------------------------------------------------
// Re-Volt Track Unit Extraction Program.
//
//-------------------------------------------------------------------------------------------------
#include "stdafx.h"

#include "OutputUnitFile.h"

extern bool FixedPoint;

U8 FloatToPSXUV(REAL value);

EndianOutputStream& operator << (EndianOutputStream& stream, REAL value)
{
	if(FixedPoint)
	{
		S32	number = (S32)( value * 65536 );

		stream.PutS32(number);
	}
	else
	{
		stream.PutFloat(value);
	}
	return stream;
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputPolySet(const RevoltPolySet* polyset)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);
	ASSERT(polyset != NULL);

	U16 polycount = polyset->size();
	*Output << polycount;

	for(U16 i = 0; i < polycount; i++)
	{
		*Output << polyset->at(i);
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputPolySets(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 polysetcount = Theme->PolySetCount();
	*Output << polysetcount;

	for(U16 i = 0; i < polysetcount; i++)
	{
		const RevoltPolySet* polyset = Theme->PolySet(i);
		OutputPolySet(polyset);
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputMesh(const RevoltMesh* mesh)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);
	ASSERT(mesh != NULL);

	U16 polysetcount = mesh->size();
	*Output << polysetcount;

	for(U16 i = 0; i < polysetcount; i++)
	{
		*Output << mesh->at(i);
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputModule(const RevoltModule* module)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);
	ASSERT(module != NULL);

	U16 instancecount = module->size();
	*Output << instancecount;

	for(U16 i = 0; i < instancecount; i++)
	{
		const RevoltTrackUnitInstance* instance = module->at(i);

		*Output << instance->UnitID;
		*Output << (U16)instance->Direction;
		*Output << instance->XPos;
		*Output << instance->YPos;
		*Output << instance->Elevation;
	}

	U16 zonecount = module->Zones.size();
	*Output << zonecount;
	for(i = 0; i < zonecount; i++)
	{
		const TRACKZONE* zone = module->Zones.at(i);

		*Output << (REAL)zone->Centre.X;
		*Output << (REAL)zone->Centre.Y;
		*Output << (REAL)zone->Centre.Z;
		*Output << (REAL)zone->XSize;
		*Output << (REAL)zone->YSize;
		*Output << (REAL)zone->ZSize;
		*Output << (REAL)zone->Links[0].Position.X;
		*Output << (REAL)zone->Links[0].Position.Y;
		*Output << (REAL)zone->Links[0].Position.Z;
		*Output << (REAL)zone->Links[1].Position.X;
		*Output << (REAL)zone->Links[1].Position.Y;
		*Output << (REAL)zone->Links[1].Position.Z;
	}
	for(U16 g = 0; g < MAX_MODULE_ROUTES; g++)
	{
		U16 ainodecount = module->Nodes[g].size();
		*Output << ainodecount;
		for(i = 0; i < ainodecount; i++)
		{
			const AINODEINFO* node = module->Nodes[g].at(i);

			*Output << (REAL)node->Ends[AI_GREEN_NODE].X;
			*Output << (REAL)node->Ends[AI_GREEN_NODE].Y;
			*Output << (REAL)node->Ends[AI_GREEN_NODE].Z;
			*Output << (REAL)node->Ends[AI_RED_NODE].X;
			*Output << (REAL)node->Ends[AI_RED_NODE].Y;
			*Output << (REAL)node->Ends[AI_RED_NODE].Z;
			*Output << (REAL)node->RacingLine;
		}
	}
	U16 lightcount = module->Lights.size();
	*Output << lightcount;
	for(i = 0; i < lightcount; i++)
	{
		const LIGHTINFO* light = module->Lights.at(i);

		*Output << (REAL)light->Position.X << (REAL)light->Position.Y << (REAL)light->Position.Z;
		*Output << (REAL)light->Reach;
		*Output << (REAL)light->Up.X << (REAL)light->Up.Y << (REAL)light->Up.Z;
		*Output << (REAL)light->Forward.X << (REAL)light->Forward.Y << (REAL)light->Forward.Z;
		*Output << (REAL)light->Cone;
		*Output << light->Red << light->Green << light->Blue;
		*Output << light->Type;
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputMeshes(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);
	
	U16 meshcount = Theme->MeshCount();
	*Output << meshcount;

	for(U16 i = 0; i < meshcount; i++)
	{
		const RevoltMesh* mesh = Theme->Mesh(i);
		OutputMesh(mesh);
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputVertices(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 vertcount = Theme->VertexCount();
	*Output << vertcount;
	for(U16 i = 0; i < vertcount; i++)
	{
		const RevoltVertex* vert = Theme->Vertex(i);
		*Output << (REAL)vert->X;
		*Output << (REAL)vert->Y;
		*Output << (REAL)vert->Z;
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputPolygons(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 polycount = Theme->PolygonCount();
	*Output << polycount;
	for(U16 i = 0; i < polycount; i++)
	{
		const RevoltPolygon* poly = Theme->Polygon(i);
		U16 vertcount = poly->size();
		*Output << max(vertcount, MIN_VERT_COUNT);
		for(U16 v = 0; v < vertcount; v++)
		{
			*Output << poly->at(v);
		}
		while(vertcount < MIN_VERT_COUNT)
		{
			*Output << ZERO_INDEX;
			vertcount++;
		}
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputRGBPolys(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 polycount = Theme->RGBPolyCount();
	*Output << polycount;
	for(U16 i = 0; i < polycount; i++)
	{
		const RevoltRGBPolygon* poly = Theme->RGBPolygon(i);
		U16 vertcount = poly->size();
		*Output << max(vertcount, MIN_VERT_COUNT);
		for(U16 v = 0; v < vertcount; v++)
		{
			*Output << poly->at(v);
		}
		while(vertcount < MIN_VERT_COUNT)
		{
			*Output << ZERO_INDEX;
			vertcount++;
		}
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputUVCoords(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 uvcount = Theme->UVCoordCount();
	*Output << uvcount;
	for(U16 i = 0; i < uvcount; i++)
	{
		const RevoltUVCoord* uv = Theme->UVCoord(i);
		if(FixedPoint)
		{
			Output->PutU8(FloatToPSXUV(uv->U));
			Output->PutU8(FloatToPSXUV(uv->V));
		}
		else
		{
			Output->PutFloat(uv->U);
			Output->PutFloat(uv->V);
		}
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputUVPolys(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 polycount = Theme->UVPolyCount();
	*Output << polycount;
	for(U16 i = 0; i < polycount; i++)
	{
		const RevoltUVPolygon* poly = Theme->UVPolygon(i);
	
		U16 vertcount = poly->size();
		*Output << max(vertcount, MIN_VERT_COUNT);
		for(U16 v = 0; v < vertcount; v++)
		{
			*Output << poly->at(v);
		}
		while(vertcount < MIN_VERT_COUNT)
		{
			*Output << ZERO_INDEX;
			vertcount++;
		}
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputUnits(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 unitcount = Theme->UnitCount();
	*Output << unitcount;
	for(U16 i = 0; i < unitcount; i++)
	{
		const RevoltTrackUnit* unit = Theme->Unit(i);
		*Output << unit->MeshID;
		
		U16 uvpolycount = unit->UVPolySet.size();

		*Output << uvpolycount;
		for(U16 n = 0; n < uvpolycount; n++)
		{
			const UVPolyInstance* poly = unit->UVPolySet.at(n);
			*Output << (const U8)(poly->TPageID | (poly->Reversed ? 0x80 : 0x00));
			*Output	<< poly->PolyID;
			*Output << poly->Rotation;
		}

		U16 surfacecount = unit->SurfaceSet.size();
		*Output << surfacecount;
		for(n = 0; n < surfacecount; n++)
		{
			*Output << unit->SurfaceSet.at(n);
		}
		*Output << (REAL)unit->PickupPos.X;
		*Output << (REAL)unit->PickupPos.Y;
		*Output << (REAL)unit->PickupPos.Z;
		U16 rgbcount = unit->RGBPolySet.size();
		*Output << rgbcount;
		for(n = 0; n < rgbcount; n++)
		{
			*Output << unit->RGBPolySet.at(n);
		}
		*Output << unit->RootEdges;
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputModules(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 modulecount = Theme->ModuleCount();
	*Output << modulecount;

	for(U16 i = 0; i < modulecount; i++)
	{
		const RevoltModule* module = Theme->Module(i);

		OutputModule(module);		
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputTPages(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 tpagecount = Theme->TPageRecnumCount();
	*Output << tpagecount;
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutput::OutputUnitFile(const string& filename, RevoltTheme* theme, EndianStream::Endian whichend)
{
	ASSERT(theme != NULL);
	
	Theme = theme;
	
	Output = new EndianOutputStream(filename.c_str(), whichend,whichend == EndianStream::ENDIAN_FOREIGN);

	if(Output != NULL)
	{
		if(Output->is_open())
		{
			if( FixedPoint )
			{
				*Output << (U32)MAKE_ID('R','T','U','P');
			}
			else
			{
				*Output << (U32)MAKE_ID('R','T','U',' ');
			}
			*Output << RTU_WRITE_VERSION;
			*Output << (U16)FOR_PC;

			OutputVertices();
			OutputPolygons();
			OutputRGBPolys();
			OutputPolySets();
			OutputMeshes();
			OutputUVCoords();
			OutputUVPolys();
			OutputUnits();
			OutputModules();
			OutputTPages();			
			*Output << theme->WallIndex;
		}
		delete Output;
		Output = NULL;
	}
	Theme = NULL;
}


UnitFileOutput::UnitFileOutput()
{
	Theme = NULL;
	Output = NULL;
}




//-------------------------------------------------------------------------------------------------
//	N64 Specific ... hmmph
//
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

class CRMapUVPoly;
typedef vector<RevoltUVCoord> RevoltUVCoords;

#define N64_FLAGS_NAREA		1
#define N64_FLAGS_DUFF		2
#define N64_FLAGS_NTPAGE	4
#define N64_FLAGS_OVER8		8

//-------------------------------------------------------------------------------------------------


class CBounds {
public:	

	int		x0,y0;
	int		x1,y1;
	int		area;
	bool	bits4;

	CBounds(void){};
	CBounds(int _x0,int _y0,int	_x1,int _y1,bool _b4)
		:x0(_x0),y0(_y0),x1(_x1),y1(_y1),bits4(_b4)
	{
		area = (x1-x0)*(y1-y0);
	};

	CBounds(const CBounds &in){
		x0 = in.x0;y0 = in.y0;x1 = in.x1;y1 = in.y1;bits4 = in.bits4;
		area = (x1-x0)*(y1-y0);
	};

	CBounds(RevoltUVCoords &in,bool b4){
		
		bits4 = b4;
		x0=y0=666666.0;
		x1=y1=0.0;
		for(int i =0;i<in.size();i++){
			RevoltUVCoord uv = in.at(i);
			if(uv.U < x0) x0 = uv.U;
			if(uv.V < y0) y0 = uv.V;
			if(uv.U > x1) x1 = uv.U;
			if(uv.V > y1) y1 = uv.V;
		}
//		if(bits4){
//			x0 -= x0&0x01;
//		}
		area = (x1-x0)*(y1-y0);
	}

	bool operator <= (int in_area){
		return area <= in_area;
	}
	bool operator>(int in_area){
		return area > in_area;
	}


	bool In(CBounds& in){

		if((in.x0 > x0)&&(in.x1 < x1)
			&&(in.y0 > y0)&&(in.y1 < y1))
				return true;

		return false;

	}

	CBounds operator+(CBounds& B){
	CBounds r;

		r.x0 = ((x0 < B.x0) ? x0 : B.x0);
		r.y0 = ((y0 < B.y0) ? y0 : B.y0);
		r.x1 = ((x1 > B.x1) ? x1 : B.x1);
		r.y1 = ((y1 > B.y1) ? y1 : B.y1);
//		if(bits4){
//			r.x0 -= r.x0&0x01;
//		}

		r.area = (r.x1-r.x0)*(r.y1-r.y0);
		return r;
	}

	void operator+=(CBounds& B){

		if(B.x0 < x0) x0 = B.x0;
		if(B.y0 < y0) y0 = B.y0;
		if(B.x1 > x1) x1 = B.x1;
		if(B.y1 > y1) y1 = B.y1;
//		if(bits4){
//			x0 -= x0&0x01;
//		}

		area = (x1-x0)*(y1-y0);
	}

};


//-------------------------------------------------------------------------------------------------

class CRMapUVPoly {
public:

	CRMapUVPoly(void):rmapped(false){}
	CRMapUVPoly(int _unitIndex,UVPolyInstance	*oI,RevoltUVCoords &oC,int _instIndex,bool b4)
	:rmapped(false),bounds(oC,b4){
	
		unitIndex		= _unitIndex;
		instIndex		= _instIndex;
		origInstance	= *oI;
		origCoords		= oC;
	}


	bool	rmapped;						//been entered in remap list
	CBounds	bounds;							//bounds of uv poly


	int					unitIndex;			//unit that used uv poly
	int					instIndex;			//index of uvPoly in unit polyset
	UVPolyInstance		origInstance;		//copy of original uvpolyInstance collected from Theme List

	U32					newTPageID;			//assigned ofter the split

	RevoltUVCoords		origCoords;			//copy of original coords  collected from Theme List
	RevoltUVCoords		newCoords;			//assigned ofter the split belong to UVCoordPool list

};

//-------------------------------------------------------------------------------------------------

class CN64TPage {
public:	
	CN64TPage(){}
	~CN64TPage(){}

	CBounds					bounds;
	int						parentID;

};

class CUVPolyGroup {
public:	

	int						ID;							//new ID for textured poly group
	CBounds					bounds;						//bounds of grou
	vector<CRMapUVPoly *>	mappedPolys;				//uvpolys mapped in group

	CUVPolyGroup(void){
		mappedPolys.clear();
	}
	CUVPolyGroup(int currID,CRMapUVPoly *uvpoly0)
	:ID(currID),bounds(uvpoly0->bounds)
	{
		mappedPolys.clear();	
	}

	CUVPolyGroup(const CUVPolyGroup &in)
	:ID(in.ID),bounds(in.bounds)
	{
		mappedPolys = in.mappedPolys;		
	}

	bool InBounds(CRMapUVPoly *uvpoly){
		return bounds.In(uvpoly->bounds);
	}

	void MarkForMap(CRMapUVPoly *poly){
		poly->rmapped = true;
		mappedPolys.push_back(poly);
	}

	// remap UV ccords
	void RemapUV(void){
	RevoltUVCoord newcoord;

		for(int i=0;i<mappedPolys.size();i++){
			
			CRMapUVPoly	*uvpoly = mappedPolys.at(i);
			uvpoly->newTPageID = ID;
			uvpoly->newCoords.clear();
			for(int j=0;j<uvpoly->origCoords.size();j++){
				const RevoltUVCoord	&coord = uvpoly->origCoords.at(j);
				newcoord.U = coord.U - bounds.x0;	
				newcoord.V = coord.V - bounds.y0;
				uvpoly->newCoords.push_back(newcoord);
			}					
		}
	}
};

//-------------------------------------------------------------------------------------------------


class CRMapTPage {
public:

	int						ID;
	int						width,height;
	int						depth;	
	vector<CUVPolyGroup>	newGroups;


	CRMapTPage(){}
	CRMapTPage(U16 I,int w,int h,int d):ID(I),width(w),height(h),depth(d){}


	void AddGroup(CUVPolyGroup &group){
		newGroups.push_back(group);
	}

	void RemapUV(void){
		for(int i=0;i<newGroups.size();i++){
			newGroups.at(i).RemapUV();
		}
	}
	
};
//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------

#include "Habextract.h"


void UnitFileOutputN64::OutputUnitFile(const string& filename, RevoltTheme* theme, EndianStream::Endian whichend)
{
	ASSERT(theme != NULL);
	
	Theme = theme;
	
	BreakTPages();
	
	Output = new EndianOutputStream(filename.c_str(), whichend,true);

	if(Output != NULL)
	{
		if(Output->is_open())
		{
			if( FixedPoint )
			{
				*Output << (U32)MAKE_ID('R','T','U','P');
			}
			else
			{
				*Output << (U32)MAKE_ID('R','T','U',' ');
			}
			*Output << RTU_WRITE_VERSION;
			*Output << (U16)FOR_PC;

			OutputVertices();
			OutputPolygons();
			OutputRGBPolys();
			OutputPolySets();
			OutputMeshes();
			OutputUVCoords();
			OutputUVPolys();
			OutputUnits();
			OutputModules();
			OutputTPages();			
			*Output << theme->WallIndex;
		}
		delete Output;
		Output = NULL;
	}
	delete[] n64TPages;

	Theme = NULL;
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------

void UnitFileOutputN64::OutputUVCoords(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 uvcount = Theme->UVCoordCount();
	*Output << uvcount;
	for(U16 i = 0; i < uvcount; i++)
	{
		const RevoltUVCoord* uv = Theme->UVCoord(i);

		Output->PutU16((U16)((2<<5)*uv->U));
		Output->PutU16((U16)((2<<5)*uv->V));
	
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutputN64::OutputVertices(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 vertcount = Theme->VertexCount();
	*Output << vertcount;
	for(U16 i = 0; i < vertcount; i++)
	{
		const RevoltVertex* vert = Theme->Vertex(i);
		*Output << (S16)(vert->X);
		*Output << (S16)(vert->Y);
		*Output << (S16)(vert->Z);
  }
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void UnitFileOutputN64::OutputUnits(void)
{
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	U16 unitcount = Theme->UnitCount();
	*Output << unitcount;
	for(U16 i = 0; i < unitcount; i++)
	{
		const RevoltTrackUnit* unit = Theme->Unit(i);
		*Output << unit->MeshID;
		
		U16 uvpolycount = unit->UVPolySet.size();

		*Output << uvpolycount;
		for(U16 n = 0; n < uvpolycount; n++)
		{
			const UVPolyInstance* poly = unit->UVPolySet.at(n);
			*Output << (const U8)(poly->TPageID | (poly->Reversed ? 0x80 : 0x00));
			*Output	<< poly->PolyID;
			*Output << poly->Rotation;
		}

		U16 surfacecount = unit->SurfaceSet.size();
		*Output << surfacecount;
		for(n = 0; n < surfacecount; n++)
		{
			*Output << unit->SurfaceSet.at(n);
		}
		*Output << (REAL)unit->PickupPos.X;
		*Output << (REAL)unit->PickupPos.Y;
		*Output << (REAL)unit->PickupPos.Z;
		U16 rgbcount = unit->RGBPolySet.size();
		*Output << rgbcount;
		for(n = 0; n < rgbcount; n++)
		{
			*Output << unit->RGBPolySet.at(n);
		}
		*Output << unit->RootEdges;

		*Output << unit->nTPages;
		for(n = 0; n < unit->nTPages; n++)
		{
			*Output << unit->TPages[n];
		}
	}
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------

void fputlong(long value, FILE *file)
{
	long	temp;

	temp = value >> 24;
	fwrite(&temp, 1, 1, file);
	temp = value >> 16;
	fwrite(&temp, 1, 1, file);
	temp = value >> 8;
	fwrite(&temp, 1, 1, file);
	fwrite(&value, 1, 1, file);
}

void fputshort(short value, FILE *file)
{
	long	temp;

	temp = value >> 8;
	fwrite(&temp, 1, 1, file);
	fwrite(&value, 1, 1, file);
}


void UnitFileOutputN64::OutputTPages(void)
{	
	ASSERT(Output != NULL);
	ASSERT(Theme != NULL);

	PS_Database& db		= ((CHabExtractApp*)AfxGetApp())->Database();

	*Output << (U16) numTPages;

	FILE *file;
	file = fopen("tpages.e64","wb");
	if(!file) return;
	fputlong(numTPages,file);

	U8* imageDest;
	imageDest = new U8[256*256];

	fputlong(0,file);		// NULL TPage 0
	fputlong(0,file);
	fputlong(0,file);


	for(int i=1;i<numTPages;i++){

		CPalette pal;
		U8*	dest;
		JC_Bitmap* bitmap	= GET_BITMAP(&db, Theme->TPageRecnum(n64TPages[i].parentID));	
		BITMAPINFO* bmi		= bitmap->CreateBitmapInfo();
		int width			= bmi->bmiHeader.biWidth;	
		int height			= bmi->bmiHeader.biHeight;			//+ve up-side-down
		int depth			= bmi->bmiHeader.biBitCount;
		U8* image			= bitmap->ImagePtr(&bmi->bmiHeader);
	
		int x0				= (int) n64TPages[i].bounds.x0;
		int y0				= (int) n64TPages[i].bounds.y0;
		int x1				= (int) n64TPages[i].bounds.x1;
		int y1				= (int) n64TPages[i].bounds.y1;
		int	div				= (depth == 4) ? 2 : 1;
		int pix_width		= (x1-x0);
		if(depth == 4)		
			pix_width		+= (0x0f&(pix_width)) ? (0x10 - (0x0f&(pix_width))) : 0;
		else if(depth == 8)
			pix_width		+= (0x07&(pix_width)) ? (0x08 - (0x07&(pix_width))) : 0;
		else{
			delete bmi;
			bitmap->Release();
			continue;
		}

		int s_width			= pix_width/div;

		dest = imageDest;
		int pix_height = 0;
		if(height > 0){

			for (int jj = 0; jj < (y1-y0); jj++)
			{
				memcpy(dest,image + ((height - y0 - jj - 1) * (width/div)) + (x0/div),s_width);
				dest+= s_width;
				pix_height++;

			}
		}else{


			height = -height;
			for (int jj = 0; jj < (y1-y0); jj++)
			{
				memcpy(dest,image + ((y0 + jj) * (width/div)) + (x0/div),s_width);
				dest+= s_width;
				pix_height++;

			}
		}

		fputlong(pix_width,file);
		fputlong(pix_height,file);
		fputlong(depth,file);




		for(U32 n=0; n < (1<<bmi->bmiHeader.biBitCount); n++)
		{
			U16 b = bmi->bmiColors[n].rgbBlue;
			U16 g = bmi->bmiColors[n].rgbGreen;
			U16 r = bmi->bmiColors[n].rgbRed;
			U16 temp = (U16)(((U16)(r >> 3) << 11) | ((U16)(g >> 3) << 6) | ((U16)(b >> 3) << 1));
			if (temp) temp |= 1;
			fputshort(temp,file);
		}

		fwrite(imageDest,dest-imageDest,1,file);

		delete bmi;
		bitmap->Release();

	}

	fclose(file);

	delete imageDest;

}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

bool UnitFileOutputN64::BreakTPages(void){

CRMapUVPoly				*uvpoly,*uvpoly0,*uvpoly1;
CRMapUVPoly				*UVPolygonIn;
vector<CRMapTPage>		TPages;

int i,j,k;
int width,height,depth;
int maxBounds;
int currID;
int n;
int TMap;
int UVPolCnt = 0;

char out[128];

	U16 unitcount = Theme->UnitCount();
	U16 TMapCount = Theme->TPageRecnumCount();
	U16 vertcount = Theme->VertexCount();

	PS_Database& db		= ((CHabExtractApp*)AfxGetApp())->Database();


	// check for bad polys
	int *BadUVPolyList = new int[TMapCount];
	memset(BadUVPolyList,0,sizeof(int)*TMapCount);

	// gather all current tmaps
	for(TMap = 1;TMap < TMapCount; TMap++){

		JC_Bitmap* bitmap	= GET_BITMAP(&db, Theme->TPageRecnum(TMap));	
		BITMAPINFO* bmi		= bitmap->CreateBitmapInfo();
		width				= bmi->bmiHeader.biWidth;
		height				= bmi->bmiHeader.biHeight;
		depth				= bmi->bmiHeader.biBitCount;
		delete bmi;
		bitmap->Release();

		if(depth > 8){
			api.Log("TMAP Over 8bit!!\n");
		}

		CRMapTPage	newBaseTPage(TMap,width,height,depth);

		TPages.push_back(newBaseTPage);
	}

	maxBounds = 4096;
	int num = 0;
	int num1 = 0;
	k=n=0;
	for(i = 0; i < unitcount; i++){
		const RevoltTrackUnit* unit		= Theme->Unit(i);		
		num								+= unit->UVPolySet.size();
	}
	UVPolygonIn = new CRMapUVPoly[num];
	num = 0;

	// extract uvpolys from units ...  not uv poly list
	for(i = 0; i < unitcount; i++){

		RevoltTrackUnit* unit		= (RevoltTrackUnit*)Theme->Unit(i);		
		U16 uvpolycount				= unit->UVPolySet.size();
		unit->Flags = 0;
		for(n = 0; n < uvpolycount; n++){

			UVPolyInstance*		polyInst	= unit->UVPolySet.at(n);
			const RevoltUVPolygon*	poly	= Theme->UVPolygon(polyInst->PolyID);

			U16 vertcount = poly->size();

			
			if(polyInst->TPageID == 0){
				width	= 256;
				height	= 256;
				depth	= 4;

			}else{
				CRMapTPage	&rTPage = TPages.at(polyInst->TPageID-1);
				width	= rTPage.width;	
				height	= rTPage.height;
				depth	= rTPage.depth;					
			}
			
			maxBounds = (depth == 4) ? 4096 : 2048;

			RevoltUVCoords	uv;
			for(U16 v = 0; v < vertcount; v++){
				RevoltUVCoord _uv = *Theme->UVCoord(poly->at(v));
				_uv.U = _uv.U*width;
				_uv.V = _uv.V*height;
				uv.push_back(_uv);			
			}

			CRMapUVPoly	ref(i,polyInst,uv,n,(depth == 4));

			if(depth > 8){
				ref.origInstance.TPageID = 0;
				ref.newTPageID = 0;							//24bit TPage
				unit->Flags		|= (U16)N64_FLAGS_OVER8;
				BadUVPolyList[polyInst->TPageID] |= (U16)N64_FLAGS_OVER8;

			}else if(polyInst->TPageID == 0){
				ref.origInstance.TPageID = 0;
				ref.newTPageID = 0;							//No TPage
				unit->Flags		|= (U16)N64_FLAGS_NTPAGE;
				BadUVPolyList[polyInst->TPageID] |= (U16)N64_FLAGS_NTPAGE;

			}
			if(ref.bounds.area == 0){
				ref.origInstance.TPageID = 0;				//No Area
				ref.newTPageID = 0;
				unit->Flags		|= (U16)N64_FLAGS_NAREA; 
				BadUVPolyList[polyInst->TPageID] |= (U16)N64_FLAGS_NAREA;

			}
			if(ref.bounds > maxBounds){
				ref.origInstance.TPageID = 0;				//Too Big
				ref.newTPageID = 0;
				unit->Flags		|= (U16)N64_FLAGS_DUFF;
				BadUVPolyList[polyInst->TPageID] |= (U16)N64_FLAGS_DUFF;
			}


			//create null coords 
			if(!ref.origInstance.TPageID){
				RevoltUVCoord newcoord;
				ref.newCoords.clear();
				for(int j=0;j<ref.origCoords.size();j++){
					newcoord.U = 0;	
					newcoord.V = 0;
					ref.newCoords.push_back(newcoord);
				}					
			}


			UVPolygonIn[UVPolCnt++] = ref;					
		}
	}
	// error info
	
	for(i=0;i<TMapCount;i++){

		if(!BadUVPolyList[i]) continue;
		
		sprintf(out,"BAD UV Polygon(s) ---- TPage %d\n",i); 
		api.Log(out);
		out[0] = 0;


		if(BadUVPolyList[i]&N64_FLAGS_OVER8){ sprintf(out,"\tOVER 8 BIT TMAP \n");	
		api.Log(out);}		
		if(BadUVPolyList[i]&N64_FLAGS_NAREA){ sprintf(out,"\tNULL AREA UV POLYGON\n");	
		api.Log(out);}		
		if(BadUVPolyList[i]&N64_FLAGS_DUFF){sprintf(out,"\tOVER SIZED UV POLYGON\n");
		api.Log(out);}
		if(BadUVPolyList[i]&N64_FLAGS_NTPAGE){sprintf(out,"\tNULL TPAGE REFERENCED\n");
		api.Log(out);}
		api.Log("\n");		

	}

	// group polys
	currID = 1;
	num = 0;
	num1 = 0;

	for(TMap = 1;TMap < TMapCount; TMap++){

		CRMapTPage	&rTPage = TPages.at(TMap-1);
		
		width	= rTPage.width;	
		height	= rTPage.height;	
		depth	= rTPage.depth;
		
		if(depth > 8) continue;

		maxBounds = (depth == 4) ? 4096 : 2048;

		for(j=0;j<UVPolCnt;j++){
			uvpoly0 = &UVPolygonIn[j];
			if(uvpoly0->rmapped) continue;
			if(uvpoly0->origInstance.TPageID != TMap) continue;
			
			CUVPolyGroup newGroup(currID,uvpoly0);
			newGroup.MarkForMap(uvpoly0);

			num++;
			num1 = 1;
			for(k=0;k<UVPolCnt;k++){
				uvpoly1 = &UVPolygonIn[k];
				if(uvpoly1->rmapped) continue;
				if(uvpoly1->origInstance.TPageID != TMap) continue;

				if( (newGroup.bounds + uvpoly1->bounds) <= maxBounds){
					newGroup.bounds += uvpoly1->bounds;
					newGroup.MarkForMap(uvpoly1);
					num++;
					num1++;
				}
			}

			rTPage.AddGroup(newGroup);

			currID++;
		}
	
		rTPage.RemapUV();
	}

	sprintf(out,"\n\nNum GROUPED UV POLYS %d\nTPages Used %d\n",(U32)num,currID-1); 
	api.Log(out);
	

	// re-map original UVPoly and insert into lists
	
	RevoltUVPolygon newUVPoly;
	Theme->ClearUVCoord();
	Theme->ClearUVPolygon();

	for(j=0;j<UVPolCnt;j++){
	
		uvpoly = &UVPolygonIn[j];
		newUVPoly.clear();
		
		for(int i=0;i<uvpoly->newCoords.size();i++){
			U32 uvindex = Theme->InsertUVCoord(uvpoly->newCoords[i]);
			newUVPoly.push_back(uvindex);
		}
		U32 uvpolyindex = Theme->InsertUVPolygon(newUVPoly);

		RevoltTrackUnit* unit			= (RevoltTrackUnit *) Theme->Unit(uvpoly->unitIndex);
		UVPolyInstance*	polyInst		= unit->UVPolySet.at(uvpoly->instIndex);
		polyInst->TPageID				= uvpoly->newTPageID;
		polyInst->PolyID				= uvpolyindex;	
	}

	// add tmap references to units

	bool *tmapRef = new bool[currID];

	for(i = 0; i < unitcount; i++){

		memset(tmapRef,0,sizeof(bool)*currID);

		RevoltTrackUnit* unit			= (RevoltTrackUnit *) Theme->Unit(i);		
		U16 uvpolycount					= unit->UVPolySet.size();
		for(n = 0; n < uvpolycount; n++){
			UVPolyInstance*		polyInst	= unit->UVPolySet.at(n);
			tmapRef[polyInst->TPageID]		= true;
		}

		

		sprintf(out,"UNIT ID : %d - \n\t",i); 
		OutputDebugString(out);


		unit->nTPages = 0;

		for(n=1;n<currID;n++){
			if(!tmapRef[n]) continue;
			unit->TPages[unit->nTPages++] = n;
			
			sprintf(out, "%4d ",n); 
			OutputDebugString(out);
		
		}

		sprintf(out,"");
		if(unit->Flags&N64_FLAGS_NAREA) sprintf(out,"N64_FLAGS_NAREA \n");	
		if(unit->Flags&N64_FLAGS_DUFF)	sprintf(out,"N64_FLAGS_DUFF \n");
		if(unit->Flags&N64_FLAGS_NTPAGE)sprintf(out,"N64_FLAGS_NTPAGE \n");
		OutputDebugString(out);


		sprintf(out,"\n\n\n"); 
		OutputDebugString(out);

	}

	delete[] tmapRef;

	// save out new tmaps

	numTPages	= 1;
	n64TPages	= new CN64TPage[currID];

	for(k = 0; k<TPages.size(); k++){

		CRMapTPage	&rTPage = TPages.at(k);

		for(i=0; i<rTPage.newGroups.size(); i++){

			CUVPolyGroup& grp				= rTPage.newGroups.at(i);
			n64TPages[numTPages].parentID	= k+1;
			n64TPages[numTPages].bounds		= grp.bounds;
			numTPages++;

		}
	}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

	for(TMap = 1;TMap < TMapCount; TMap++){

		sprintf(out,"MAPPED PAGES ID : %d - \n",TMap); 
		OutputDebugString(out);
		

		for(k = 0; k<numTPages; k++){
		
			if(n64TPages[k].parentID == TMap){

				sprintf(out, "\t%4d , (%8d,%8d)(%8d,%8d) %8d\n",k, 
					n64TPages[k].bounds.x0,n64TPages[k].bounds.y0,
					n64TPages[k].bounds.x1,n64TPages[k].bounds.y1,
					n64TPages[k].bounds.area);	
				
				OutputDebugString(out);
			}
							
		}

		sprintf(out,"\n\n\n"); 
		OutputDebugString(out);
	}


	delete[] UVPolygonIn;
	delete[] BadUVPolyList;


	return true;

}
