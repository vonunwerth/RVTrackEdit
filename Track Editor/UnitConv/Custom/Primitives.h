#ifndef _PRIMITIVES_H
#define _PRIMITIVES_H

#include "constants.h"
#include "structs.h"
#include "MappedVector.h"
#include "NamedLists.h"
#include <vector>

using namespace std;

typedef struct
{
    #ifdef _PSX
        unsigned short Red:5;
        unsigned short Green:5;
        unsigned short Blue:5;
        unsigned short Transparent:1;
    #else
        U8 Red;
        U8 Green;
        U8 Blue;
        U8 Transparent;
    #endif
}RevoltRGB;


//-------------------------------------------------------------------------------------------------
// RevoltVertexCompare
//
// Function object used by the (MappedVector based) VertexMap as a 'less<>' predicate
// Returns a bool to indicate whether 'left' is deemed to be less than 'right'
// The X coord is used as the major key followed by the Y & Z coords respectively.
//-------------------------------------------------------------------------------------------------
class RevoltVertexCompare
{
	public:
		bool operator()(const RevoltVertex* left, const RevoltVertex* right) const
		{
			if(left->X == right->X)
			{
				if(left->Y == right->Y)
				{
					if(left->Z == right->Z)
					{
						return false;
					}
					else
					{
						return (left->Z < right->Z);
					}
				}
				else
				{
					return (left->Y < right->Y);
				}
			}
			else
			{
				return (left->X < right->X);
			}
		}
};

//Define a class to contain a unique set of RevoltVertex objects
typedef MappedVector<RevoltVertex, RevoltVertexCompare> VertexMap;

//-------------------------------------------------------------------------------------------------
// RevoltPolygon
//
// Represents a polygon as a list of indices into a vector of vertices
//-------------------------------------------------------------------------------------------------
typedef SmallIndexFirstVector RevoltPolygon;

typedef struct 
{
    U16 Width;
    U16 Height;
    U8  BitDepth;
    U8  PaletteSize;
}RevoltTPageHeader;

typedef struct
{
    RevoltTPageHeader Header;
    RevoltRGB		  Palette[256];
    U8                Image[1];
}RevoltTPage;

//-------------------------------------------------------------------------------------------------
// RevoltUVPolygon
//
// Represents a UV polygon as a list of indices into a vector of UV coords
//-------------------------------------------------------------------------------------------------
typedef SmallIndexFirstVector RevoltUVPolygon;

typedef MappedVector<RevoltUVPolygon, IndexVectorCompare> UVPolygonMap;

//-------------------------------------------------------------------------------------------------
// RevoltUVCoordCompare
//
// Function object used by the (MappedVector based) UVPolygonMap as a 'less<>' predicate
// Returns a bool to indicate whether 'left' is deemed to be less than 'right'
// Sorts by 'u' component, then by 'v' component
//-------------------------------------------------------------------------------------------------
class RevoltUVCoordCompare
{
	public:
		bool operator()(const RevoltUVCoord* left, const RevoltUVCoord* right) const
		{
			if(left->U == right->U)
			{
				return (left->V < right->V);
			}
			return (left->U < right->U);
		}
};

typedef MappedVector<RevoltUVCoord, RevoltUVCoordCompare> UVCoordMap;

typedef struct
{
	U8		TPageID;
	INDEX   PolyID;
	U8	    Rotation;
	bool	Reversed;
}UVPolyInstance;

typedef PointerVector<UVPolyInstance> UVPolyInstanceVector;
typedef vector<U8> SurfaceVector;
typedef vector<INDEX> RGBPolyIndexVector;
//-------------------------------------------------------------------------------------------------
// RGBPolygon
//
// Represents a Gouraud polygon as a list of indices into a vector of RGB values (
//-------------------------------------------------------------------------------------------------
typedef vector<U32> RevoltRGBPolygon;

//-------------------------------------------------------------------------------------------------
// RGBVectorCompare
//
// Function object usable as a sorting callback function
// Returns (left < right)
//-------------------------------------------------------------------------------------------------
class RGBPolygonCompare
{
	public:
		bool operator()(const RevoltRGBPolygon* left, const RevoltRGBPolygon* right) const
		{
			if(left->size() == right->size())
			{
				U32 pos = 0;
				while(pos < left->size() && ((*left)[pos] == (*right)[pos]))
				{
					pos++;
				}
				if(pos >= left->size())
				{
					return false;
				}
				return ((*left)[pos] < (*right)[pos]);
			}
			return (left->size() < right->size());
		}
};

typedef MappedVector<RevoltRGBPolygon, RGBPolygonCompare> RGBPolygonMap;

class RevoltTrackUnit
{
	public:
	    INDEX MeshID;  /* Index into the Mesh Array */
		UVPolyInstanceVector UVPolySet; /* Indices into TrackUnitSet::UVPolygonSets */
		SurfaceVector		 SurfaceSet; /* list of surface materials for each collision poly */
		RevoltVertex		 PickupPos;
		RGBPolyIndexVector   RGBPolySet;
		U16					 RootEdges;

		U16					 nTPages;		// Used for N64 only
		U16					 Flags;
		U8					 TPages[64];	// Used for N64 only
};

typedef PointerVector<TRACKZONE> TrackZoneVector;

typedef PointerVector<AINODEINFO> AINodeInfoVector;

typedef PointerVector<LIGHTINFO> LightInfoVector;

class RevoltModule : public NamedPointerVector<RevoltTrackUnitInstance>
{
	public:
		RevoltModule(const string& name);
		TrackZoneVector Zones;
		AINodeInfoVector Nodes[MAX_MODULE_ROUTES];	//one set of nodes for each of the possible routes through the module
		LightInfoVector	 Lights;
};

//-------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------
typedef MappedVector<RevoltPolygon, IndexVectorCompare> PolygonMap;
typedef NamedVector<U16> RevoltPolySet;
typedef NamedVector<U16> RevoltMesh;
typedef PointerVector<RevoltMesh> MeshVector;
typedef PointerVector<RevoltTrackUnit> RevoltTrackUnitSet;
typedef PointerVector<RevoltModule> RevoltModuleSet;
typedef MappedVector<RevoltPolySet, IndexVectorCompare> PolySetMap;

class U32Compare
{
	public:
		bool operator()(const U32* left, const U32* right) const
		{
			return (*left < *right);
		}
};

typedef MappedVector<U32, U32Compare> TPageRecnumMap;

class RevoltTheme
{
	protected:
		string				TheName;
		VertexMap			VertexPool;
		PolygonMap			PolygonPool;
		MeshVector			MeshPool;
		RevoltModuleSet		ModulePool;
		RevoltTrackUnitSet  UnitPool;
		UVCoordMap			UVCoordPool;
		UVPolygonMap		UVPolygonPool;
		TPageRecnumMap		TPageRecnumPool;
		RGBPolygonMap		RGBPolygonPool;
		PolySetMap			PolySetPool;

	public:

		RevoltTheme(const string& name){TheName = name;};

		U16 VertexCount(void){return VertexPool.size();};
		U16 PolygonCount(void){return PolygonPool.size();};
		U16 MeshCount(void){return MeshPool.size();};
		U16 ModuleCount(void){return ModulePool.size();};
		U16 UnitCount(void){return UnitPool.size();};
		U16 UVCoordCount(void){return UVCoordPool.size();};
		U16 UVPolyCount(void){return UVPolygonPool.size();};
		U16 RGBPolyCount(void){return RGBPolygonPool.size();};
		U16 TPageRecnumCount(void){return TPageRecnumPool.size();};
		U16 PolySetCount(void){return PolySetPool.size();};

		const RevoltVertex*       Vertex(U16 index){return VertexPool[index];};
		const RevoltPolygon*      Polygon(U16 index){return PolygonPool[index];};
		const RevoltMesh*		  Mesh(U16 index){return MeshPool[index];};
		const RevoltModule*	      Module(U16 index){return ModulePool[index];};
		const RevoltTrackUnit*    Unit(U16 index){return UnitPool[index];};
		const RevoltUVCoord*	  UVCoord(U16 index){return UVCoordPool[index];};
		const RevoltUVPolygon*	  UVPolygon(U16 index){return UVPolygonPool[index];};
		const RevoltRGBPolygon*	  RGBPolygon(U16 index){return RGBPolygonPool[index];};
		U32						  TPageRecnum(U32 index){return *(TPageRecnumPool[index]);};
		const RevoltPolySet*	  PolySet(U16 index){return PolySetPool[index];};

		U16 InsertVertex(const RevoltVertex& vertex){return VertexPool.Insert(vertex);};
		U16 InsertPolygon(const RevoltPolygon& polygon){return PolygonPool.Insert(polygon);};
		U16 InsertMesh(const RevoltMesh* mesh){return MeshPool.Insert(mesh);};
		U16 InsertModule(const RevoltModule* module){return ModulePool.Insert(module);};
		U16 InsertUnit(const RevoltTrackUnit* unit){return UnitPool.Insert(unit);};
		U16 InsertUVCoord(const RevoltUVCoord& uvcoord){return UVCoordPool.Insert(uvcoord);};
		U16 InsertUVPolygon(const RevoltUVPolygon& uvpoly){return UVPolygonPool.Insert(uvpoly);};
		U32 InsertTPageRecnum(U32 recnum){return TPageRecnumPool.Insert(recnum);};
		U16 InsertRGBPolygon(const RevoltRGBPolygon& rgbpoly){return RGBPolygonPool.Insert(rgbpoly);};
		U16 InsertPolySet(const RevoltPolySet& polyset){return PolySetPool.Insert(polyset);};

		void ClearUVCoord(void){UVCoordMap	cls;UVCoordPool = cls;};
		void ClearUVPolygon(void){UVPolygonMap cls;UVPolygonPool = cls;};




		U16 WallIndex;	//index of the first unit which represents a wall
};

#endif