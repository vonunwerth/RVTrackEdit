// UpdateTrack.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <endstrm.h>
#include "pcincl.h"

const int MAX_DESCRIPTION_LENGTH = 40;

#ifdef _DEBUG
	const char LevelFolder[] = "..\\game\\levels";
#else
	const char LevelFolder[] = "..\\levels";
#endif

unsigned long GetMemChecksum(long *mem, long bytes)
{
	long crc, bits, i, flag;
	long *pos;

// calc crc

	bytes >>= 2;

	crc = 0xffffffff;
	pos = mem;
	for ( ; bytes ; bytes--, pos++)
	{
		bits = *pos;
		for (i = 32 ; i ; i--)
		{
			flag = crc & 0x80000000;
			crc  = (crc << 1) | (bits & 1);
			bits >>= 1;

			if (flag)
			{
				crc ^= 0x04c11db7;	// polynomial
			}
		}
	}

	crc ^= 0xffffffff;

// return crc

	return crc;
}

void MakeChksumText(char* text, long* mem, long bytes)
{
	long chksum = GetMemChecksum(mem, bytes);
	for(int i=0; i<8; i++)
	{
		text[i] = ((chksum >> 28) & 0x0f) + 'a';
		chksum <<= 4;
	}
}

bool GetFileDescription(const char* filename, char* description)
{
	assert(description != NULL);

	bool success = false;
	EndianInputStream is(filename);
	if(is.is_open())
	{
		U32 fileid = is.GetU32();
		if(fileid == MAKE_ID('T','D','F',' '))
		{
			is.GetU16(); //skip version info
			is.GetPaddedText(description, MAX_DESCRIPTION_LENGTH - 1);
			success = true;
		}
	}
	return success;
}

void RenameTDF(void)
{
	char filename[_MAX_PATH];
	char description[MAX_DESCRIPTION_LENGTH];
	char newfilename[_MAX_PATH];
	WIN32_FIND_DATA find_data;

	HANDLE h = FindFirstFile((LPCTSTR)"TDF\\FILE????.TDF", &find_data);
	if(h != INVALID_HANDLE_VALUE)
	{
		do
		{
			sprintf(filename, "TDF\\%s", find_data.cFileName); 
			printf("%s\r", filename);
			if(GetFileDescription(filename, description) == true)
			{
				char* space;
				do
				{
					space = strchr(description, ' ');
					if(space != NULL)
					{
						*space = '_';
					}
				}while(space != NULL);
				sprintf(newfilename,"TDF\\!%s.TDF", description);
				rename(filename, newfilename);
			}
			else
			{
				printf("\n");
			}
		}while((FindNextFile(h, &find_data) == TRUE));
		FindClose(h);
	}
}

bool GetLevelDescription(const char* levelname, char* description)
{
	assert(description != NULL);

	char linebuffer[MAX_DESCRIPTION_LENGTH + 40];
	static char nametoken[] = "NAME "; //note - the trailing space is important
	
	char infospec[MAX_PATH];
	sprintf(infospec, "%s\\%s\\%s.inf", LevelFolder, levelname, levelname);

	bool success = false;
	FILE* fi;
	fi = fopen(infospec, "rt");
	if(fi != NULL)
	{
		bool done = false;
		while((done == false) && (success == false))
		{
			char* lineptr = fgets(linebuffer, sizeof(linebuffer), fi);
			if(lineptr == NULL)
			{	
				done = true;
			}
			else
			{
				if(strnicmp(nametoken, linebuffer, strlen(nametoken)) == 0)
				{
					char* openquote = strchr(linebuffer, '\'');
					if(openquote != NULL)
					{
						char* dash = strchr(openquote+1, '-');
						if(dash != NULL)
						{
							char* closequote = strchr(dash+1, '\'');
							if(closequote != NULL)
							{
								*closequote = '\0';
								strncpy(description, dash+1, MAX_DESCRIPTION_LENGTH - 1);
								success = true;
							}
						}
					}
				}
			}
		}
		fclose(fi);
	}
	return success;
}

void RenameCompiledTracks(void)
{
	char levelname[_MAX_PATH];
	char filename[_MAX_PATH];
	char description[MAX_DESCRIPTION_LENGTH];
	char newfoldername[_MAX_PATH];
	char oldfoldername[_MAX_PATH];
	char newfilename[_MAX_PATH];
	char oldfilename[_MAX_PATH];
	char chksum[9] = "12345678";	//important to init string to its full length
	WIN32_FIND_DATA d_find_data;
	WIN32_FIND_DATA f_find_data;

	sprintf(levelname, "%s\\USER???", LevelFolder);
	HANDLE dh = FindFirstFile(levelname, &d_find_data);
	if(dh != INVALID_HANDLE_VALUE)
	{
		do
		{
			memset(description, '\0', MAX_DESCRIPTION_LENGTH);
			GetLevelDescription(d_find_data.cFileName, description);
			char* space;
			do
			{
				space = strchr(description, ' ');
				if(space != NULL)
				{
					*space = '_';
				}
			}while(space != NULL);
			MakeChksumText(chksum, (long*)description, MAX_DESCRIPTION_LENGTH);
			sprintf(filename, "%s\\%s\\%s*.*", LevelFolder, d_find_data.cFileName, d_find_data.cFileName); 
			sprintf(newfoldername, "%s\\USER_%s", LevelFolder, chksum);
			sprintf(oldfoldername, "%s\\%s", LevelFolder, d_find_data.cFileName);
			HANDLE fh = FindFirstFile(filename, &f_find_data);
			if(fh != INVALID_HANDLE_VALUE)
			{
				printf("%s  %s\r\n", oldfoldername, description);
				printf("%s\r\n", newfoldername);
				rename(oldfoldername, newfoldername);
				do
				{
					sprintf(newfilename, "%s\\USER_%s\\USER_%s%s", LevelFolder, chksum, chksum, &f_find_data.cFileName[strlen(d_find_data.cFileName)]);
					sprintf(oldfilename, "%s\\USER_%s\\%s", LevelFolder, chksum, f_find_data.cFileName);
					rename(oldfilename, newfilename);
				}while((FindNextFile(fh, &f_find_data) == TRUE));
				FindClose(fh);
			}
			else
			{
				RemoveDirectory(oldfoldername);
			}
		}while((FindNextFile(dh, &d_find_data) == TRUE));
		FindClose(dh);
	}
}

int main(int argc, char* argv[])
{
	printf("Re-Volt Track Editor file update tool\n\r");
	printf("-------------------------------------\n\r\n\r");
	RenameTDF();
	RenameCompiledTracks();
	printf("\n\rUpdate complete - Press ENTER key to exit.\n\r");
	getchar();
	return 0;
}
