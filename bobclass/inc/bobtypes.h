//******************************************************
// bobtypes.h
//
// compiler-specific var types for fixed data sizes
//******************************************************

#ifndef __BOBTYPES_H
#define __BOBTYPES_H

//#define __BCPLUSPLUS__
#ifdef _ORIGINAL_SOURCE
#define _MSC_VER = 1
#endif

#ifdef __BCPLUSPLUS__			//need different values for Watcom, etc

#if defined(__DPMI32__)
//fixed data sizes:
#define UINT8	unsigned char	//8 bit
#define UINT16	unsigned short	//16 bit
#define UINT32	unsigned long	//32 bit
#define INT8	char	//8 bit
#define INT16	short	//16 bit
#define INT32	long	//32 bit

typedef char					 CHAR;
typedef unsigned char 		UCHAR;
typedef signed long         LONG;
typedef unsigned long		ULONG;
typedef int						  INT;
typedef unsigned int        UINT;

#define BIGPTR UCHAR*
#define FARPTR unsigned char*


#else	//__DPMI32__ !defined

//fixed data sizes:
#define UINT8	unsigned char	//8 bit
#define UINT16	unsigned int	//16 bit
#define UINT32	unsigned long	//32 bit
#define INT8	char	//8 bit
#define INT16	int		//16 bit
#define INT32	long	//32 bit

#define U8	unsigned char	//8 bit
#define U16	unsigned int	//16 bit
#define U32	unsigned long	//32 bit
#define S8	char	//8 bit
#define S16	int		//16 bit
#define S32	long	//32 bit

#define MAX_UINT8 0xFF
#define MAX_UINT16 0xFFFF
#define MAX_UINT32 0xFFFFFFFF

#define MAX_U8 0xFF
#define MAX_U16 0xFFFF
#define MAX_U32 0xFFFFFFFF

typedef char					 CHAR;
typedef unsigned char 		UCHAR;
typedef signed long         LONG;
typedef unsigned long		ULONG;
typedef int						  INT;
typedef unsigned int        UINT;

#define BIGPTR unsigned char huge * //UINT8 huge*
#define FARPTR unsigned char far*	//UINT8 far*

#endif	//__DPMI32__

//#define FALSE 0
//#define TRUE 1

#endif //__BCPLUSPLUS__

#if defined (_MSC_VER)

#if !defined(MAKE_ID)
#define MAKE_ID(a,b,c,d) (ULONG) (((ULONG)(d)<<24)|((ULONG)(c)<<16)|((ULONG)(b)<<8)|((ULONG)(a)))
#endif

//#ifndef BOOL
//#define BOOL	int
//#define FALSE 0
//#define TRUE 1
//#endif

typedef unsigned char	UINT8;
typedef signed char		INT8;
typedef unsigned short	UINT16;
typedef signed short	INT16;

#ifndef _BASETSD_H_
typedef unsigned int	UINT32;
typedef signed int		INT32;
#endif

typedef unsigned char	U8;
typedef signed char		S8;
typedef unsigned short	U16;
typedef signed short	S16;
typedef unsigned long	U32;
typedef signed long		S32;

#define MAX_UINT8	UCHAR_MAX
#define MAX_UINT16	USHRT_MAX
#define MAX_UINT32	UINT_MAX

#define MAX_INT8	SCHAR_MAX
#define MAX_INT16	SHRT_MAX
#define MAX_INT32	INT_MAX

#define MIN_INT8	SCHAR_MIN
#define MIN_INT16	SHRT_MIN
#define MIN_INT32	INT_MIN

#define MAX_U8	UCHAR_MAX
#define MAX_U16	USHRT_MAX
#define MAX_U32	UINT_MAX

#define MAX_S8	SCHAR_MAX
#define MAX_S16	SHRT_MAX
#define MAX_S32	INT_MAX

#define MIN_S8	SCHAR_MIN
#define MIN_S16	SHRT_MIN
#define MIN_S32	INT_MIN

#if 0
//fixed data sizes:
#define UINT8	unsigned char	//8 bit
#define UINT16	unsigned short	//16 bit
#define UINT32	unsigned long	//32 bit
#define INT8	char	//8 bit
#define INT16	short	//16 bit
#define INT32	long	//32 bit

#define MAX_UINT8 0xFF
#define MAX_UINT16 0xFFFF
#define MAX_UINT32 0xFFFFFFFF

typedef char					 CHAR;
typedef unsigned char 		UCHAR;
typedef signed long         LONG;
typedef unsigned long		ULONG;
typedef int						  INT;
typedef unsigned int        UINT;
#endif

#define BIGPTR UCHAR*
#define FARPTR unsigned char*

#endif //defined (_MSC_VER)
#endif
